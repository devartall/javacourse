package lesson7.list.self;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class ArrayStore<T> implements Store<T> {

    public static final int DEFAULT_SIZE = 10;
    public static final int RATE = 2;
    private Object[] store;
    private int size;

    private int modCount;

    public ArrayStore() {
        this.store = new Object[DEFAULT_SIZE];
    }

    public ArrayStore(int capacity) {
        this.store = new Object[capacity];
    }

    @Override
    public void add(T value) {
        if (size == store.length) increase();

        store[size++] = value;

        modCount++;
    }

    private void increase() {
        Object[] newStore = new Object[store.length * RATE];

        for (int i = 0; i < store.length; i++) {
            newStore[i] = store[i];
        }

        store = newStore;
    }

    @Override
    public T get(int index) {
        return (T) store[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T remove(int index) {
        return null;
    }

    private void decrease() {

    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private int current = 0;
            private int expectedModCount = modCount;

            @Override
            public boolean hasNext() {
                return current < size;
            }

            @Override
            public T next() {
                if (expectedModCount != modCount) throw new ConcurrentModificationException();

                return (T) store[current++];
            }
        };
    }

    public static void main(String[] args) {
        ArrayStore<Integer> store = new ArrayStore<>();

        for (int i = 0; i < ArrayStore.DEFAULT_SIZE; i++) {
            store.add(i + 1);
        }

        store.add(11);

        for (int i = 0; i < store.size(); i++) {
            System.out.print(store.get(i) + " ");
        }

        System.out.println();

        for (Integer i : store) {
            System.out.print(i + " ");
        }

        System.out.println();
        for (Integer i : store) {
            if (i == 5) {
                store.add(100);
                break;
            }
        }

        Iterator<Integer> iterator = store.iterator();

        while (iterator.hasNext()) {
            Integer i = iterator.next();
            if (i == 4) {
                store.add(200);
                break;
            }
        }

        iterator.next();
    }
}
