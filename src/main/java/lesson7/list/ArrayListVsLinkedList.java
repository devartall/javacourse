package lesson7.list;

import java.util.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-25
 */
public class ArrayListVsLinkedList {

    /**
     * Вставка в начало: ArrayDeque
     */
    static void addFirst_ArrayList(int iterations) {
        Integer STUB = 1;

        ArrayList<Integer> list = new ArrayList<>();
        long before = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++) {
            list.add(0, STUB);
        }
        System.out.println(System.currentTimeMillis() - before);
    }
    static void addFirst_LinkedList(int iterations) {
        Integer STUB = 1;

        LinkedList<Integer> list = new LinkedList<>();
        long before = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++) {
            list.addFirst(STUB);
        }
        System.out.println(System.currentTimeMillis() - before);
    }
    static void addFirst_ArrayDeque(int iterations) {
        Integer STUB = 1;

        Deque<Integer> deque = new ArrayDeque<>(iterations);
        long before = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++) {
            deque.addFirst(STUB);
        }
        System.out.println(System.currentTimeMillis() - before);
    }

    /**
     * Вставка в конец: ArrayList
     */
    static void addLast_ArrayList(int iterations) {
        Integer STUB = 1;

        ArrayList<Integer> list = new ArrayList<>();
        long before = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++) {
            list.add(STUB);
        }
        System.out.println(System.currentTimeMillis() - before);
    }
    static void addLast_LinkedList(int iterations) {
        Integer STUB = 1;

        LinkedList<Integer> list = new LinkedList<>();
        long before = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++) {
            list.add(STUB);
        }
        System.out.println(System.currentTimeMillis() - before);
    }

    /**
     * Вставка в середину: ArrayList
     */
    static void addMid_ArrayList(int iterations) {
        Integer STUB = 1;

        ArrayList<Integer> list = new ArrayList<>(iterations);
        long before = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++) {
            list.add(list.size() / 2, STUB);
        }
        System.out.println(System.currentTimeMillis() - before);
    }
    static void addMid_LinkedList(int iterations) {
        Integer STUB = 1;

        LinkedList<Integer> list = new LinkedList<>();
        long before = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++) {
            list.add(list.size() / 2, STUB);
        }
        System.out.println(System.currentTimeMillis() - before);
    }

    /**
     * Iterator: LinkedList
     */
    static void iteratorRemove_ArrayList(int iterations) {
        ArrayList<Integer> list = new ArrayList<>(iterations);
        for (int i = 1; i <= iterations; i++) {
            list.add(i);
        }

        Iterator<Integer> iterator = list.iterator();

        long before = System.currentTimeMillis();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            if (next % 2 == 0)
                iterator.remove();
        }
        System.out.println(System.currentTimeMillis() - before);
    }
    static void iteratorRemove_LinkedList(int iterations) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 1; i <= iterations; i++) {
            list.add(i);
        }

        Iterator<Integer> iterator = list.iterator();

        long before = System.currentTimeMillis();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            if (next % 2 == 0)
                iterator.remove();
        }
        System.out.println(System.currentTimeMillis() - before);
    }

    /**
     * removeIf: LinkedList
     */
    static void removeIf_ArrayList(int iterations) {
        ArrayList<Integer> list = new ArrayList<>(iterations);
        for (int i = 1; i <= iterations; i++) {
            list.add(i);
        }

        long before = System.currentTimeMillis();
        list.removeIf(integer -> integer % 2 == 0);
        System.out.println(System.currentTimeMillis() - before);
    }
    static void removeIf_LinkedList(int iterations) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 1; i <= iterations; i++) {
            list.add(i);
        }

        long before = System.currentTimeMillis();
        list.removeIf(integer -> integer % 2 == 0);
        System.out.println(System.currentTimeMillis() - before);
    }


    /**
     * removeAll: ArrayList
     */
    static void removeAll_ArrayList(int iterations) {
        ArrayList<Integer> list = new ArrayList<>(iterations);
        for (int i = 1; i <= iterations; i++) {
            list.add(i);
        }

        ArrayList<Integer> even = new ArrayList<>(iterations);
        for (int i = 2; i <= iterations; i += 2) {
            list.add(i);
        }

        long before = System.currentTimeMillis();
        list.removeAll(even);
        System.out.println(System.currentTimeMillis() - before);
    }
    static void removeAll_LinkedList(int iterations) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 1; i <= iterations; i++) {
            list.add(i);
        }

        LinkedList<Integer> even = new LinkedList<>();
        for (int i = 2; i <= iterations; i += 2) {
            list.add(i);
        }

        long before = System.currentTimeMillis();
        list.removeAll(even);
        System.out.println(System.currentTimeMillis() - before);
    }


    /**
     * retainAll: ArrayList
     */
    static void retainAll_ArrayList(int iterations) {
        ArrayList<Integer> list = new ArrayList<>(iterations);
        for (int i = 1; i <= iterations; i++) {
            list.add(i);
        }

        ArrayList<Integer> even = new ArrayList<>(iterations);
        for (int i = 2; i <= iterations; i += 2) {
            list.add(i);
        }

        long before = System.currentTimeMillis();
        list.retainAll(even);
        System.out.println(System.currentTimeMillis() - before);
    }
    static void retainAll_LinkedList(int iterations) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 1; i <= iterations; i++) {
            list.add(i);
        }

        LinkedList<Integer> even = new LinkedList<>();
        for (int i = 2; i <= iterations; i += 2) {
            list.add(i);
        }

        long before = System.currentTimeMillis();
        list.retainAll(even);
        System.out.println(System.currentTimeMillis() - before);
    }


    /**
     * remove middle: ArrayList
     */
    static void removeMid_ArrayList(int iterations) {
        ArrayList<Integer> list = new ArrayList<>(iterations);
        for (int i = 1; i <= iterations; i++) {
            list.add(i);
        }

        long before = System.currentTimeMillis();
        while (!list.isEmpty()) {
            list.remove(list.size() / 2);
        }
        System.out.println(System.currentTimeMillis() - before);
    }
    static void removeMid_LinkedList(int iterations) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 1; i <= iterations; i++) {
            list.add(i);
        }

        long before = System.currentTimeMillis();
        while (!list.isEmpty()) {
            list.remove(list.size() / 2);
        }
        System.out.println(System.currentTimeMillis() - before);
    }

    public static void main(String[] args) {
        //addFirst_ArrayList(100_000);
        //addFirst_LinkedList(10_000_000);
        //addFirst_ArrayDeque(10_000_000);



        //addLast_ArrayList(10_000_000);
        //addLast_LinkedList(10_000_000);



        //addMid_ArrayList(100_000);
        //addMid_LinkedList(100_000);



        //iteratorRemove_ArrayList(100_000);
        //iteratorRemove_LinkedList(100_000);



        //removeIf_ArrayList(1_000_000);
        //removeIf_LinkedList(1_000_000);



        //removeAll_ArrayList(10_000_000);
        //removeAll_LinkedList(10_000_000);



        //retainAll_ArrayList(10_000_000);
        //retainAll_LinkedList(10_000_000);



        //removeMid_ArrayList(100_000);
        //removeMid_LinkedList(100_000);
    }
}
