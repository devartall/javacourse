package lesson7.list;

import java.util.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-24
 */
class IntegerGenerator implements Iterable<Integer> {

    private int count;
    private int bound;

    public IntegerGenerator(int count, int bound) {
        this.count = count;
        this.bound = bound;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getBound() {
        return bound;
    }

    public void setBound(int bound) {
        this.bound = bound;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            private Random random = new Random();

            @Override
            public boolean hasNext() {
                return count-- > 0;
            }

            @Override
            public Integer next() {
                return random.nextInt(bound);
            }
        };
    }
}

public class IteratorExample {
    public static void main(String[] args) {
        IntegerGenerator generator = new IntegerGenerator(10, 100);

        StringJoiner joiner = new StringJoiner(", ", "[", "]");
        for (Integer i : generator) {
            joiner.add(Integer.toString(i));
        }

        System.out.println(joiner);
    }
}
