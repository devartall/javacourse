package lesson7.list;

import lesson4.Utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Изучение класса ArrayList
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-15
 */
public class ArrayListTraining {

    /**
     * Возвращает текущую ёмкость экземпляра ArrayList
     */
    static int getCapacity(ArrayList list) throws Exception {
        Field elementDataField = list.getClass().getDeclaredField("elementData");
        elementDataField.setAccessible(true);

        return ((Object[])elementDataField.get(list)).length;
    }

    /**
     * Возвращает n-ую ёмкость экземпляра ArrayList
     */
    static int nextCapacity(int initialCapacity, int n) throws Exception {
        int capacity = initialCapacity;

        for (int i = 0; i < n; i++)
            capacity = capacity + capacity / 2;

        return capacity;
    }

    /**
     * Возвращает последовательность ёмкостей экземпляра ArrayList относительно начальной ёмкости
     */
    static int[] getCapacities(int initialCapacity) throws Exception {
        List<Integer> list = new ArrayList<>();

        long capacity = initialCapacity;

        while (capacity <= Integer.MAX_VALUE) {
            list.add((int)capacity);
            capacity = capacity + capacity / 2;
        }

        int[] capacities = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            capacities[i] = list.get(i);
        }

        return capacities;
    }

    /**
     * Выводит в консоль размер и ёмкость экземпляра ArrayList
     */
    static void printSizeAndCapacity(ArrayList list) throws Exception {
        System.out.println("SIZE: " + list.size() + "   CAPACITY: " + getCapacity(list));
    }

    /**
     * Альтернатива System.arrayCopy
     */
    static void manualCopy(int[] source) {
        int[] copy = new int[source.length];

        for (int i = 0; i < source.length; i++) {
            copy[i] = source[i] + 10;
        }
    }

    /**
     * Оценка performance "ручного" копирования массива
     * Итерации необходимы для "разогрева" JVM
     */
    static void test_manualCopy(int iterations) {
        int[] array = Utils.randomArray(100_000_000, 100_000_000);

        long sum = 0;
        for (int i = 0; i < iterations; i++) {
            //long before = System.nanoTime();
            long before = System.currentTimeMillis();
            manualCopy(array);
            //long after = System.nanoTime() - before;
            long after = System.currentTimeMillis() - before;
            System.out.println(after);
            sum += after;
        }

        System.out.println("Average: " + sum / iterations);
    }

    /**
     * Оценка performance System.arrayCopy
     * Итерации необходимы для "разогрева" JVM
     */
    static void test_systemArrayCopy(int iterations) {
        int[] array = Utils.randomArray(100_000_000, 100_000_000);

        long sum = 0;
        for (int i = 0; i < iterations; i++) {
            //long before = System.nanoTime();
            long before = System.currentTimeMillis();
            System.arraycopy(array, 0, new int[array.length], 0, array.length);
            //long after = System.nanoTime() - before;
            long after = System.currentTimeMillis() - before;
            System.out.println(after);
            sum += after;
        }

        System.out.println("Average: " + sum / iterations);
    }

    /**
     * Создаем destination-массив заранее
     */
    static void manualCopy_2(int[] source, int[] dest) {
        for (int i = 0; i < source.length; i++)
            dest[i] = source[i];
    }
    static void test_manualCopy_2(int iterations) {
        int[] source = new int[100_000_000];
        int[] dest = new int[source.length];

        long sum = 0;
        for (int i = 0; i < iterations; i++) {
            long before = System.nanoTime();
            //long before = System.currentTimeMillis();
            manualCopy_2(source, dest);
            long after = System.nanoTime() - before;
            //long after = System.currentTimeMillis() - before;
            System.out.println(after);
            sum += after;
        }

        System.out.println("Average: " + sum / iterations);
    }

    /**
     * Создаем destination-массив заранее
     */
    static void test_systemArrayCopy_2(int iterations) {
        int[] source = new int[100_000_000];
        int[] dest = new int[source.length];

        long sum = 0;
        for (int i = 0; i < iterations; i++) {
            long before = System.nanoTime();
            //long before = System.currentTimeMillis();
            System.arraycopy(source, 0, dest, 0, source.length);
            long after = System.nanoTime() - before;
            //long after = System.currentTimeMillis() - before;
            System.out.println(after);
            sum += after;
        }

        System.out.println("Average: " + sum / iterations);
    }

    /**
     * Тестирование метода add класса ArrayList
     * Метод показывает изменение ёмкости экземпляра ArrayList и выделенной памяти для JVM
     * Варианты запуска:
     *  test_add(1_796_357_452, 1_796_357_452);
     *  test_add(10, 1_796_357_452);
     */
    static void test_add(int capacity, int iterations) throws Exception {
        System.out.println("Memory before creating the instance: " + Runtime.getRuntime().freeMemory() / 1024 / 1024 + " Mb");
        ArrayList<Integer> list = new ArrayList<>(capacity);
        System.out.println("Memory after creating the instance:  " + Runtime.getRuntime().freeMemory() / 1024 / 1024 + " Mb");

        Field elementDataField = list.getClass().getDeclaredField("elementData");
        elementDataField.setAccessible(true);

        //Без заглушки память будет забиваться гораздо быстрее
        Integer STUB = 1;

        System.out.println("Adding data...");

        long before = System.currentTimeMillis();
        long sub_before = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++) {
            if (list.size() == ((Object[])elementDataField.get(list)).length) {
                System.out.println("Start copy...");
                list.add(STUB);
                System.out.println("   New capacity: " + ((Object[])elementDataField.get(list)).length);
                System.out.println("End copy...");
            } else {
                list.add(STUB);
            }

            if (list.size() % 100_000_000 == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("   ");
                sb.append("Size: ").append(list.size()).append("   ");
                sb.append("Capacity: ") .append(getCapacity(list)).append("   ");
                sb.append("Free memory: ").append(Runtime.getRuntime().freeMemory() / 1024 / 1024).append(" Mb   ");
                sb.append(System.currentTimeMillis() - sub_before).append(" ms");
                System.out.println(sb);
                sub_before = System.currentTimeMillis();
            }
        }
        System.out.println((int)(System.currentTimeMillis() - before) + " ms");

        System.out.println("Done.");
    }

    /**
     * Тестирование метода trimToSize класса ArrayList
     */
    static void test_trimToSize() throws Exception {
        int capacity = 1_000_000;

        System.out.println("Before creation:  " + Runtime.getRuntime().freeMemory() / 1024 / 1024 + " Mb");
        ArrayList<Integer> list = new ArrayList<>(Integer.MAX_VALUE - 8);
        for (int i = 0; i < capacity / 2; i++) {
            list.add(i);
        }

        printSizeAndCapacity(list);

        System.out.println("After creation:   " + Runtime.getRuntime().freeMemory() / 1024 / 1024 + " Mb");
        list.trimToSize();

        System.out.println("Garbage collection...");
        for (int i = 0; i < 10; i++) {
            System.gc();
            Thread.sleep(1000L);
        }

        System.out.println("After trimToSize: " + Runtime.getRuntime().freeMemory() / 1024 / 1024 + " Mb");
        printSizeAndCapacity(list);
    }

    /**
     * ArrayList.clear() VS. new ArrayList(capacity)
     */
    static void test_clear() {
        int capacity = 100_000_000;

        Integer POSITIVE = 1;
        ArrayList<Integer> list = new ArrayList<>(capacity);
        for (int i = 0; i < capacity; i++) {
           list.add(POSITIVE);
        }

        Integer NEGATIVE = -1;
        long before = System.currentTimeMillis();
        list.clear();
        for (int i = 0; i < capacity; i++) {
            list.add(NEGATIVE);
        }
        System.out.println(System.currentTimeMillis() - before);

        Integer SQUARE = 2 * 2;
        before = System.currentTimeMillis();
        list = new ArrayList<>(capacity);
        for (int i = 0; i < capacity; i++) {
            list.add(SQUARE);
        }
        System.out.println(System.currentTimeMillis() - before);
    }

    /**
     * ArrayList.ensureCapacity
     *
     * Увеличивает ёмкость листа если новая ёмкость больше текущей
     */
    static void test_ensureCapacity() throws Exception {
        int capacity = 100;

        ArrayList<Integer> list = new ArrayList<>(capacity);
        for (int i = 0; i < capacity; i++) {
            list.add(i);
        }

        printSizeAndCapacity(list);

        list.ensureCapacity(2 * list.size());

        printSizeAndCapacity(list);
    }

    /**
     * Наглядная демонстрация методов ArrayList.removeAll, ArrayList.retainAll, ArrayList.removeIf
     */
    static void test_RemoveAll_And_RetainAll_And_RemoveIf() {
        int capacity = 10;

        ArrayList<Integer> source = new ArrayList<>(capacity);
        for (int i = 1; i <= capacity; i++) {
            source.add(i);
        }
        ArrayList<Integer> even = new ArrayList<>(capacity);
        for (int i = 1; i <= capacity; i++) {
            if (i % 2 == 0)
                even.add(i);
        }

        System.out.print("After removeAll(): ");
        ArrayList<Integer> remove = new ArrayList<>(source);
        remove.removeAll(even);
        System.out.println(Arrays.toString(remove.toArray(new Integer[0])));

        System.out.print("After retainAll(): ");
        ArrayList<Integer> retain = new ArrayList<>(source);
        retain.retainAll(even);
        System.out.println(Arrays.toString(retain.toArray(new Integer[0])));

        System.out.print("After removeIf():  ");
        ArrayList<Integer> removeIf = new ArrayList<>(source);
        removeIf.removeIf(integer -> integer % 2 == 0);
        System.out.println(Arrays.toString(removeIf.toArray(new Integer[0])));
    }

    /**
     * ArrayList.replaceAll
     */
    static void test_replaceAll() {
        int capacity = 10;

        ArrayList<Integer> list = new ArrayList<>(capacity);
        for (int i = 1; i < capacity; i++) {
            list.add(i);
        }

        System.out.println("Before replaceAll(): " + Arrays.toString(list.toArray(new Integer[0])));
        list.replaceAll(integer -> integer * 10);
        System.out.println("After replaceAll():  " + Arrays.toString(list.toArray(new Integer[0])));
    }

    /**
     * ArrayList.sort
     */
    static void test_sort() {
        int capacity = 10;

        ArrayList<Integer> list = new ArrayList<>(capacity);

        int[] array = Utils.randomArray(capacity, capacity);
        for (int i = 1; i < capacity; i++) {
            list.add(array[i]);
        }

        System.out.println("Before sort(): " + Arrays.toString(list.toArray(new Integer[0])));

        list.sort((o1, o2) -> {
            if (o1 % 2 == 0 && o2 % 2 != 0)
                return -1;
            if (o1 % 2 != 0 && o2 % 2 == 0)
                return 1;

            return o1 - o2;
        });

        System.out.println("After sort():  " + Arrays.toString(list.toArray(new Integer[0])));
    }

    public static void main(String[] args) throws Exception {
        /*
            Для более достоверной оценки методы test_manualCopy и test_systemArrayCopy
            рекомендуется запускать по отдельности т.е. с перезапуском JVM
        */
        //System.out.println("System.arrayCopy VS. ManualCopy");
        //test_manualCopy(100);
        //test_systemArrayCopy(100);
        //test_manualCopy_2(50);
        //test_systemArrayCopy_2(50);



        /*
            Последовательность ёмкостей
         */
        //System.out.println(Arrays.toString(getCapacities(10)));







        //test_add(1_796_357_452, 1_796_357_452);
        //test_add(10, 1_796_357_452);



        //test_trimToSize();



        //test_clear();



        //test_ensureCapacity();



        //test_RemoveAll_And_RetainAll_And_RemoveIf();



        //test_replaceAll();



        test_sort();
    }
}
