package lesson7.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class ConcurrentModificationException {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            list.add(i);
        }

        System.out.println(Arrays.toString(list.toArray()));

        System.out.println();

        //OK
        /*for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == 2)
                list.remove(i);
        }*/

        //ConcurrentModificationException
        /*for (Integer i : list)
            if (i == 2)
                list.remove(i);*/

        //ConcurrentModificationException
        /*Iterator<Integer> iterator = list.iterator();
        list.add(6);
        if (iterator.hasNext()) iterator.next();*/

        //OK
        /*Iterator<Integer> iterator = list.iterator();
        list.add(6);
        for (Integer i : list)
            System.out.print(i + " ");*/

        CopyOnWriteArrayList<Integer> list1 = new CopyOnWriteArrayList<>();
        for (int i = 1; i <= 5; i++) {
            list1.add(i);
        }

        for (Integer i : list1) {
            if (i == 2)
                list1.remove(i);

        }

        System.out.println();
        for (int i : list1)
            System.out.print(i + " ");
    }
}
