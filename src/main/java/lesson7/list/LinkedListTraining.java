package lesson7.list;

import java.util.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-19
 */
public class LinkedListTraining {

    /**
     * Queue
     */
    static void queue_example() {
        System.out.println("Queue example (LinkedList): ");

        System.out.print("In:  ");
        StringJoiner inJoiner = new StringJoiner(", ", "[", "]");
        Queue<Integer> queue = new LinkedList<>();
        for (int i = 1; i <= 5; i++) {
            queue.add(i);
            inJoiner.add(Integer.toString(i));
        }
        System.out.println(inJoiner);

        System.out.print("Out: ");
        StringJoiner outJoiner = new StringJoiner(", ", "[", "]");
        while (!queue.isEmpty())
            outJoiner.add(Integer.toString(queue.poll()));

        System.out.println(outJoiner);


        //or
        System.out.println();


        System.out.println("Queue example (ArrayDeque): ");
        System.out.print("In:  ");
        inJoiner = new StringJoiner(", ", "[", "]");
        Queue<Integer> arrayQueue = new ArrayDeque<>();
        for (int i = 1; i <= 5; i++) {
            arrayQueue.add(i);
            inJoiner.add(Integer.toString(i));
        }
        System.out.println(inJoiner);

        System.out.print("Out: ");
        outJoiner = new StringJoiner(", ", "[", "]");
        while (!arrayQueue.isEmpty())
            outJoiner.add(Integer.toString(arrayQueue.poll()));

        System.out.println(outJoiner);
    }

    /**
     * Stack
     */
    static void stack_example() {
        System.out.println("Stack example (LinkedList): ");

        System.out.print("In:  ");
        StringJoiner inJoiner = new StringJoiner(", ", "[", "]");
        Deque<Integer> deque = new LinkedList<>();
        for (int i = 1; i <= 5; i++) {
            deque.add(i);
            inJoiner.add(Integer.toString(i));
        }
        System.out.println(inJoiner);

        System.out.print("Out: ");
        StringJoiner outJoiner = new StringJoiner(", ", "[", "]");
        while (!deque.isEmpty())
            outJoiner.add(Integer.toString(deque.removeLast()));

        System.out.println(outJoiner);


        //or
        System.out.println();


        System.out.println("Stack example (ArrayDeque): ");
        System.out.print("In:  ");
        inJoiner = new StringJoiner(", ", "[", "]");
        Deque<Integer> arrayDeque = new ArrayDeque<>();
        for (int i = 1; i <= 5; i++) {
            arrayDeque.add(i);
            inJoiner.add(Integer.toString(i));
        }
        System.out.println(inJoiner);

        System.out.print("Out: ");
        outJoiner = new StringJoiner(", ", "[", "]");
        while (!arrayDeque.isEmpty())
            outJoiner.add(Integer.toString(arrayDeque.removeLast()));

        System.out.println(outJoiner);
    }

    public static void main(String[] args) {
        //queue_example();


        stack_example();
    }
}
