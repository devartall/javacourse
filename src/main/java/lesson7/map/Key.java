package lesson7.map;

/**
 * НЕправильная реализация контракта между equals и hashCode
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-25
 */
public class Key {

    private int value;

    public Key(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null) return false;
        if (!getClass().equals(obj.getClass())) return false;

        Key key = (Key) obj;

        return value == key.value;
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
