package lesson7.map;

import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.function.Function;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-24
 */
public class HashMapTraining {

    static void test_put(Map<Key, Integer> map, Key key, Integer value) {
        long before = System.nanoTime();
        map.put(key, value);
        System.out.println(System.nanoTime() - before);
    }

    static void test_merge() {
        Map<Integer, String> map = createMap();

        map.forEach((key, value) -> map.merge(key, "_end", (s1, s2) -> s1 + s2));

        printMap(map);
    }

    static void test_compute() {
        Map<Integer, String> map = createMap();

        map.forEach((key, value) -> map.compute(key, (integer, s) -> s + "_end"));

        printMap(map);
    }

    static void test_computeIfAbsent() {
        Map<Integer, String> map = createMap();

        map.computeIfAbsent(4, new Function<Integer, String>() {
            @Override
            public String apply(Integer integer) {
                return "_end";
            }
        });

        printMap(map);
    }

    private static Map<Integer, String> createMap() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "s1");
        map.put(2, "s2");
        map.put(3, "s3");

        return map;
    }

    private static void printMap(Map<Integer, String> map) {
        StringJoiner joiner = new StringJoiner(", ", "[", "]");
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            joiner.add(entry.getKey() + " = " + entry.getValue());
        }

        System.out.println(joiner);
    }

    public static void main(String[] args) {
        /*
        Map<Key, Integer> map1 = new HashMap<>(10);
        for (int i = 1; i <= 10; i++) {
            test_put(map1, new Key(i), i);
        }
        */



        //test_merge();



        //test_compute();



        //test_computeIfAbsent();
    }
}
