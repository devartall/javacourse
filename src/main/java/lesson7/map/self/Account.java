package lesson7.map.self;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Account {

    private BigDecimal amount;

    public Account(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "amount=" + amount +
                '}';
    }

    public static void main(String[] args) {
        System.out.println(0.1 + 0.2);

        BigDecimal first = BigDecimal.valueOf(0.15);
        BigDecimal second = BigDecimal.valueOf(0.2);

        BigDecimal result = first.setScale(1, RoundingMode.HALF_EVEN).add(second);
        System.out.println(result);
    }
}
