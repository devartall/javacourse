package lesson4.algorithm.sort;

import lesson4.Person;

import java.util.Arrays;

/**
 * Демонстрация свойства устойчивости сортировок
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-14
 */
public class TimVsQuick {

    static void quickSort(Person[] array) {
        quickSort(array, 0, array.length - 1);
    }

    static void quickSort(Person[] array, int low, int high) {
        if (low < high) {
            int part = partition(array, low, high);

            quickSort(array, low, part - 1);
            quickSort(array, part + 1, high);
        }
    }

    private static int partition(Person[] array, int low, int high) {
        int pivot = array[high].getAge();
        int i = low - 1;

        for (int j = low; j < high; j++)
            if (array[j].getAge() < pivot) {
                i++;

                Person temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }

        Person temp = array[i + 1];
        array[i + 1] = array[high];
        array[high] = temp;

        return i + 1;
    }

    public static Person[] createPersons() {
        Person[] persons = new Person[10];

        persons[0] = new Person("Name_0", 9);
        persons[1] = new Person("Name_1", 8);
        persons[2] = new Person("Name_2", 7);
        persons[3] = new Person("Name_3", 6);
        persons[4] = new Person("Name_4", 5);
        persons[5] = new Person("Name_5", 7);
        persons[6] = new Person("Name_6", 3);
        persons[7] = new Person("Name_7", 2);
        persons[8] = new Person("Name_8", 7);
        persons[9] = new Person("Name_9", 0);

        return persons;
    }

    public static void main(String[] args) {
        Person[] persons = createPersons();

        for (int i = 0; i < persons.length; i++)
            System.out.print(persons[i].getName() + ", ");
        System.out.println();

        Arrays.sort(persons);

        for (int i = 0; i < persons.length; i++)
            System.out.print(persons[i].getName() + ", ");
        System.out.println();

        System.out.println("---------------");

        persons = createPersons();
        for (int i = 0; i < persons.length; i++)
            System.out.print(persons[i].getName() + ", ");
        System.out.println();

        quickSort(persons);

        for (int i = 0; i < persons.length; i++)
            System.out.print(persons[i].getName() + ", ");
        System.out.println();
    }
}
