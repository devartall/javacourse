package lesson4.algorithm.sort;

import lesson4.Utils;

/**
 * Сортировка вставками
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-14
 */
public class InsertionSort {

    static void sort(int array[]) {
        for (int i = 1; i < array.length; i++) {
            int key = array[i];
            int j = i - 1;

            while (j >= 0 && array[j] > key) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = key;
        }
    }

    public static void main(String args[]) {
        int size = 1_000_000;

        int[] array = Utils.randomArray(size);

        /*Utils.printArray(array);

        InsertionSort.sort(array);

        Utils.printArray(array);*/

        int[] sorted = new int[size];
        for (int i = 0; i < sorted.length; i++) {
            sorted[i] = i;
        }

        sorted[sorted.length / 2] = 0;

        long before = System.currentTimeMillis();
        InsertionSort.sort(sorted);
        System.out.println(System.currentTimeMillis() - before);
    }
}
