package lesson4.algorithm.sort;

import lesson4.Utils;

/**
 * Сортировка слиянием
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-13
 */
public class MergeSort {

    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    private void sort(int[] array, int left, int right) {
        if (left < right) {
            int mid = (left + right) / 2;

            sort(array, left, mid);
            sort(array , mid + 1, right);

            merge(array, left, mid, right);
        }
    }

    private void merge(int array[], int left, int mid, int right) {
        int s1 = mid - left + 1;
        int s2 = right - mid;

        int sa1[] = new int[s1];
        int sa2[] = new int[s2];

        for (int i = 0; i < s1; i++)
            sa1[i] = array[left + i];

        for (int j = 0; j < s2; j++)
            sa2[j] = array[mid + 1 + j];

        int i = 0, j = 0, k = left;

        while (i < s1 && j < s2)
            if (sa1[i] <= sa2[j])
                array[k++] = sa1[i++];
            else
                array[k++] = sa2[j++];

        while (i < s1)
            array[k++] = sa1[i++];

        while (j < s2)
            array[k++] = sa2[j++];
    }

    public static void main(String args[]) {
        int size = 1_000_000;

        int[] array = Utils.randomArray(size, 2 + size);

        //Utils.printArray(array);

        MergeSort obj = new MergeSort();
        obj.sort(array);

        //Utils.printArray(array);
    }
}
