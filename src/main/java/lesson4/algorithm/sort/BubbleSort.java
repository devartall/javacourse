package lesson4.algorithm.sort;

import lesson4.Utils;

/**
 * Сортировка пузырьком
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-14
 */
public class BubbleSort {

    public static void sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {

            boolean isSorted = true;

            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    swap(arr, j, j + 1);
                    isSorted = false;
                }
            }

            if (isSorted) break;
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        int[] array = Utils.randomArray(100);

        Utils.printArray(array);

        BubbleSort.sort(array);

        Utils.printArray(array);

        /*int[] array = new int[100_000];

        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }

        long before = System.currentTimeMillis();
        sort(array);
        System.out.println(System.currentTimeMillis() - before);*/
    }
}
