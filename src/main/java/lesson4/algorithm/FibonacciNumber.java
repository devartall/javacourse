package lesson4.algorithm;

/**
 * Вычисление чисел Фибоначчи
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-14
 */
public class FibonacciNumber {

    static int find(int number) {
        if (number == 0) return 0;
        if (number == 1) return 1;

        return find(number - 1) + find(number - 2);
    }

    static int find2(int number) {
        int[] dp = new int[number + 1];
        dp[1] = 1;

        for (int i = 2; i <= number; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }

        return dp[number];
    }

    public static void main(String[] args) {
        //System.out.println(find(45));
        System.out.println(find2(10000));
    }
}
