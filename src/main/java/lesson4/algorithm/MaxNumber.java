package lesson4.algorithm;

import lesson4.Utils;

/**
 * Поиск максимального числа
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-14
 */
public class MaxNumber {

    static int max(int[] arr) {
        if (arr.length == 0) throw new IllegalArgumentException();

        int max = arr[0];
        for (int i = 1; i < arr.length; i++)
            if (max < arr[i]) max = arr[i];

        return max;
    }

    public static void main(String[] args) {
        int[] array = Utils.randomArray(10, 100);
        System.out.println(max(array));
    }
}
