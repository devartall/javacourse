package lesson4.algorithm;

import lesson4.Utils;

/**
 * Бинарный поиск
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-14
 */
public class BinarySearch {

    public static int lineary(int[] array, int value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value)
                return i;
        }

        return -1;
    }

    public static int binary(int[] array, int value) {
        int left = 0, right = array.length - 1;

        while (left <= right) {
            int mid = (left + right) / 2;

            if (array[mid] < value) {
                left = mid + 1;
            } else if (array[mid] > value) {
                right = mid - 1;
            } else {
                return mid;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int[] array = Utils.randomArray(100_000_000, 100_000_000);

        long before = System.currentTimeMillis();
        binary(array, 6);
        binary(array, 67);
        System.out.println(System.currentTimeMillis() - before);

        before = System.currentTimeMillis();
        lineary(array, 6);
        lineary(array, 67);
        System.out.println(System.currentTimeMillis() - before);
    }
}
