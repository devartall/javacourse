package lesson4.task;

import java.util.Random;

/*
    Найти наибольшую общую подстроку
    (все символы в нижнем регистре, пробелов нет)

    Пример:
        - на вход:  "rjqobjsberlstbk", "jqgqkvpssberkgo"
        - на выход: "sber"

    Пример:
        - на вход:  "abcd", "efg"
        - на выход: "-1"
 */
public class LongestCommonSubstring {

    //more fast than first
    public static String solution3(String s1, String s2) {
        int start = 0, end = 0;

        for (int i = 0; i < s1.length(); i++) {
            for (int j = 0; j < s2.length(); j++) {
                if (s1.charAt(i) == s2.charAt(j)) {
                    int m = i + 1, n = j + 1;
                    for (; m < s1.length() && n < s2.length(); m++, n++)
                        if (s1.charAt(m) != s2.charAt(n))
                            break;

                    if (end - start < m - i) {
                        start = i;
                        end = m;
                    }
                }
            }
        }

        return end - start != 0 ? s1.substring(start, end) : "-1";
    }

    private static String[] searchAllSubstrings(String s) {
        int size = ((1 + s.length()) * s.length()) / 2;
        System.out.println("Size:" + size);
        String[] substrs = new String[size];

        int k = 0;
        for (int i = 0; i < s.length(); i++)
            for (int j = i; j < s.length(); j++)
                substrs[k++] = s.substring(i, j + 1);

        return substrs;
    }

    //naive (brute-force)
    public static String solution2(String s1, String s2) {
        String[] arr1 = searchAllSubstrings(s1);
        String[] arr2 = searchAllSubstrings(s2);

        String max = "";

        for (int i = 0; i < arr1.length; i++)
            for (int j = 0; j < arr2.length; j++)
                if (arr1[i].equals(arr2[j]) && max.length() < arr1[i].length())
                    max = arr1[i];

        return max.isEmpty() ? "-1" : max;
    }

    //fast
    public static String solution(String s1, String s2) {
        int[][] matrix = new int[s1.length() + 1][s2.length() + 1];

        int im = 0, jm = 0;
        for (int i = 0; i < s1.length(); i++) {
            for (int j = 0; j < s2.length(); j++) {
                if (s1.charAt(i) == s2.charAt(j)) {
                    matrix[i + 1][j + 1] = matrix[i][j] + 1;

                    if (matrix[im][jm] < matrix[i + 1][j + 1]) {
                        im = i + 1;
                        jm = j + 1;
                    }
                }
            }
        }

        //printMatrix(s1, s2, matrix);

        return (matrix[im][jm] != 0) ? s1.substring(im - matrix[im][jm], im) : "-1";
    }

    private static void printMatrix(String s1, String s2, int[][] matrix) {
        System.out.print("   ");
        for (int i = 0; i < s2.length(); i++) {
            System.out.print(" " + s2.charAt(i));
        }
        System.out.println();

        for (int i = 0; i < matrix.length; i++) {
            if (i == 0) System.out.print("  ");
            else System.out.print(s1.charAt(i - 1) + " ");
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static String generateString(int size) {
        char[] chars = new char[size];
        Random random = new Random();

        for (int i = 0; i < size; i++) {
            //chars[i] = (char) (random.nextInt(26) + 'a');
            chars[i] = 'a';
        }

        return new String(chars);
    }

    public static void main(String[] args) {
        //String result = solution("rjqobjsberlstbk", "jqgqkvpssberkgo");
        //System.out.println("Substring: " + result);

        int size = 5_000;
        long before = System.currentTimeMillis();
        String s1 = generateString(size - (int)(Math.random() * (size * 0.8)));
        String s2 = generateString(size - (int)(Math.random() * (size * 0.8)));
        System.out.println("Time for generation: " + (System.currentTimeMillis() - before));

        System.out.println("-------------");
        System.out.println("Input strings:");
        //System.out.println(s1);
        //System.out.println(s2);

        System.out.println("-------------");

        before = System.currentTimeMillis();
        solution(s1, s2);
        System.out.println("Time for solution : " + (System.currentTimeMillis() - before));

        //before = System.currentTimeMillis();
        //solution2(s1, s2);
        //System.out.println("Time for solution2: " + (System.currentTimeMillis() - before));

        before = System.currentTimeMillis();
        solution3(s1, s2);
        System.out.println("Time for solution3: " + (System.currentTimeMillis() - before));
    }
}
