package lesson4.task;

/*
    Является ли строка палиндромом?
    (символы могут быть любыми, но регистр и пробелы не учитывать)

    Пример:
        - на вход:  заказ
        - на выход: true

    Пример:
        - на вход:  заказы
        - на выход: false

    Пример:
        - на вход:  И к вам и трем с смерти мавки
        - на выход: true
 */
public class PalindromeCheck {

    public static boolean solution(String str) {
        return false;
    }
}
