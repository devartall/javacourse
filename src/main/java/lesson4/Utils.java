package lesson4;

import java.util.Random;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-14
 */
public class Utils {

    public static final String COMMA = ", ";

    private static Random random = new Random();

    public static int[] randomArray(int size) {
        if (size < 0) throw new IllegalArgumentException();

        return randomArray(size, size);
    }

    public static int[] randomArray(int size, int bound) {
        if (size < 0) throw new IllegalArgumentException();

        int[] array = new int[size];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(bound);
        }

        return array;
    }

    public static int randomInt(int bound) {
        return random.nextInt(bound);
    }

    public static void printArray(int[] array) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < array.length; i++)
            sb.append(array[i]).append(COMMA);

        System.out.println(sb);
    }
}
