package lesson4.datastructure;

/**
 * Связный список
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-13
 */
public class LinkList {

    private Node first;
    private Node last;
    private int size;

    class Node {
        int value;
        Node next;
        Node prev;

        Node(int value) {
            this.value = value;
        }
    }

    public void insert(int value) {
        Node node = new Node(value);

        if (isEmpty()) {
            first = node;
            last = node;
        } else {
            last.next = node;
            node.prev = last;
            last = node;
        }

        size++;
    }

    public int removeLast() {
        if (isEmpty()) throw new RuntimeException("List is empty");

        int value = last.value;

        last = last.prev;

        if (last != null)
            last.next = null;
        else
            first = null;

        size--;

        return value;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void print() {
        StringBuilder sb = new StringBuilder();

        Node cur = first;

        while (cur != null) {
            sb.append(cur.value).append(", ");
            cur = cur.next;
        }

        System.out.println(sb.toString());
    }

    public static void main(String[] args) {
        LinkList list = new LinkList();
        list.insert(1);
        list.insert(2);
        list.insert(3);
        list.insert(4);
        list.insert(5);

        list.print();
    }
}
