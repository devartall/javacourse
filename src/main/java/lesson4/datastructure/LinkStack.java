package lesson4.datastructure;

import lesson4.Utils;

/**
 * АСД Stack на базе связного списка
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-14
 */
public class LinkStack {

    private LinkList store = new LinkList();

    public void push(int value) {
        store.insert(value);
    }

    public int pop() {
        if (isEmpty()) throw new RuntimeException("Stack is empty");

        return store.removeLast();
    }

    public int size() {
        return store.size();
    }

    public boolean isEmpty() {
        return store.isEmpty();
    }

    public static void main(String[] args) {
        LinkStack stack = new LinkStack();

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 5; i++) {
            int value = Utils.randomInt(10);
            stack.push(value);
            sb.append(value).append(Utils.COMMA);
        }

        sb.append("\n");

        for (int i = 0; i < 5; i++) {
            sb.append(stack.pop()).append(Utils.COMMA);
        }

        System.out.println(sb);
    }
}
