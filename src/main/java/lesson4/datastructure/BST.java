package lesson4.datastructure;

import java.util.Random;

/**
 * Бинарное дерево поиска
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-13
 */
public class BST {
    private Node root;

    class Node {
        int value;
        Node left;
        Node right;

        Node(int value) {
            this.value = value;
        }
    }

    public void add(int value) {
        root = add(root, value);
    }

    private Node add(Node node, int value) {
        if (node == null) return new Node(value);

        if (value >= node.value)
            node.right = add(node.right, value);
        else
            node.left = add(node.left, value);

        return node;
    }

    public static boolean compareTrees(BST t1, BST t2) {
        return compareTrees(t1.root, t2.root);
    }

    private static boolean compareTrees(Node r1, Node r2) {
        if (r1 == null && r2 == null) return true;
        if (r1 != null && r2 == null) return false;
        if (r1 == null && r2 != null) return false;
        if (r1.value != r2.value) return false;

        return compareTrees(r1.left, r2.left) && compareTrees(r1.right, r2.right);
    }

    //Прямой
    public void preOrder() {
        preOrder(root);
        System.out.println();
    }
    private void preOrder(Node root) {
        if (root != null) {
            System.out.print(root.value + ", ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    //Симметричный
    public void inOrder() {
        inOrder(root);
        System.out.println();
    }
    private void inOrder(Node root) {
        if (root != null) {
            inOrder(root.left);
            System.out.print(root.value + ", ");
            inOrder(root.right);
        }
    }

    //Обратный
    public void postOrder() {
        postOrder(root);
        System.out.println();
    }
    private void postOrder(Node root) {
        if (root != null) {
            postOrder(root.left);
            postOrder(root.right);
            System.out.print(root.value + ", ");
        }
    }

    public int height() {
        return height(root, 0);
    }
    private int height(Node root, int height) {
        if (root == null) return height;

        int left = height(root.left, height + 1);
        int right = height(root.right, height + 1);

        return Math.max(left, right);
    }

    public static void main(String[] args) {
        BST bst_1 = new BST();
        BST bst_2 = new BST();

        Random random = new Random();

        for (int i = 0; i < random.nextInt(50); i++) {
            int value = random.nextInt(100);

            bst_1.add(value);
            bst_2.add(value);
        }

        System.out.println(compareTrees(bst_1, bst_2));
        System.out.println(bst_1.height());

        BST bst = new BST();
        bst.add(5);
        bst.add(3);
        bst.add(2);
        bst.add(4);
        bst.add(1);
        bst.add(7);
        bst.add(6);
        bst.add(8);
        bst.add(9);

        bst.preOrder();
        bst.inOrder();
        bst.postOrder();
    }
}
