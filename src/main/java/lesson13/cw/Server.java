package lesson13.cw;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 * Простой TCP сервер
 */
public class Server {
    public static void main(String[] args) {
        if (args.length < 1) return;
        while (true) {
            int port = Integer.parseInt(args[0]);

            try (ServerSocket serverSocket = new ServerSocket(port)) {
                Socket socket = serverSocket.accept();
                System.out.println("New client connected");

                OutputStream outputStream = socket.getOutputStream();
                PrintWriter writer = new PrintWriter(outputStream, true);
                writer.println("Answer at " + new Date().toString());

                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String clientSentence = inFromClient.readLine();
                System.out.println("Received from client: " + clientSentence);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
