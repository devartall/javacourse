package lesson13.cw;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client {
    public static void main(String [] args) throws IOException {
        String sentence;
        String newSentence;
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        newSentence = inFromUser.readLine();
        Socket clientSocket = new Socket("localhost", 8999);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());

        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        sentence = inFromServer.readLine();
        outToServer.writeBytes(newSentence);

        System.out.println("Received from server: " + sentence);
        clientSocket.close();
    }

}
