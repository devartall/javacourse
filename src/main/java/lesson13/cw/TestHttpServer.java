package lesson13.cw;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class TestHttpServer {

        public static void main(String[] args) throws Exception {
            HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
            server.createContext("/test", new TestServerHandler());
            server.setExecutor(null);
            server.start();
        }

        static class TestServerHandler implements HttpHandler {
            @Override
            public void handle(HttpExchange t) throws IOException {
                String response = "<h3>hello from test server</h3>";
                t.getResponseHeaders().set("Content-Type", "text/html");
                t.sendResponseHeaders(200, response.getBytes().length);
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
            }
        }
}
