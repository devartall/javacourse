package lesson10.cw;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ParallelStreamExample {

    public static void main(String[] args) {

        streamBuilderExample();
        System.out.println("Usual stream:");


        IntStream range = IntStream.rangeClosed(1, 10);
        long startTime = new Date().getTime();
        range.forEach(System.out::println);
        long endTime = new Date().getTime();

        System.out.println("Execution time:" + (endTime - startTime) + "\nParallel:");


        IntStream range2 = IntStream.rangeClosed(1, 10);
        startTime = new Date().getTime();
        range2.parallel().forEach(System.out::println);
        endTime = new Date().getTime();
        System.out.println("Execution time:" + (endTime - startTime));

        IntStream range3 = IntStream.rangeClosed(1, 1000000000);
        startTime = new Date().getTime();
        range3.parallel().reduce(Integer.MIN_VALUE, Math::max);
        endTime = new Date().getTime();
        System.out.println("Parallel reduce time: " + (endTime - startTime));


        IntStream range4 = IntStream.rangeClosed(1, 1000000000);
        startTime = new Date().getTime();
        range4.reduce(Integer.MIN_VALUE, Math::max);
        endTime = new Date().getTime();

        System.out.println("Normal reduce time: " + (endTime - startTime));

        mapAndFlatMapExample();

    }

    public static void streamBuilderExample() {
        Stream.Builder<Integer> streamBuider = Stream.<Integer>builder()
                .add(0)
                .add(1);
        for (int i = 2; i <= 8; i += 2) {
            streamBuider.accept(i);
        }
        streamBuider
                .add(9)
                .add(10)
                .build()
                .forEach(System.out::println);
    }

    public static void mapAndFlatMapExample() {
        List<Integer> integers = Arrays.asList(1, 3, 5, 7, 9);
        integers.stream().map(integer -> {
            if (integer % 2 == 1) {
                integer++;
            }
            return integer;
        }).forEach(System.out::println);

        integers.stream().map(integer -> {
            if (integer % 2 == 1) {
                integer++;
            }
            return integer;
        }).flatMap(i -> Stream.of(i, i + 1))
          .forEach(System.out::println);
    }

}
