package lesson10.cw;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Класс для работы с лямбдами
 */
public class LambdaLogic {

    public static void main(String... args) {
        LambdaLogic lambdaLogic = new LambdaLogic();
        lambdaLogic.predicateExample();
        lambdaLogic.binaryOperatorTest();
        lambdaLogic.unaryOperatorExample();
        lambdaLogic.consumerDemo();

        lambdaLogic.zeroOperator();
        lambdaLogic.oneOperator();
        lambdaLogic.twoOperators();

        lambdaLogic.rangeExample();
        IntStream.of(1, 2).forEach(integer -> System.out.println(integer));
        IntStream.of(1, 2).forEach(System.out::println);
        IntStream.of(1, 2).forEach(LambdaLogic::printTest);

        lambdaLogic.iterateByStream();
        lambdaLogic.streamReduceExample();
        lambdaLogic.minValueByStream();
        lambdaLogic.maxValueByStream();
    }

    public void predicateExample() {
        Predicate<String> predicate0 = str -> str.startsWith("A");

        Predicate<String> predicate1 =  new Predicate<String>() {
            @Override
            public boolean test(String str) {
                return str.endsWith("B");
            }
        };

        String firstString = "Another string";
        boolean predicate0TestString = predicate0.test(firstString);
        boolean predicate1TestString = predicate1.test(firstString);

        Predicate<String> combination = predicate0.and(predicate1.negate());

        boolean anotherTestingResult = combination.test("Another string  ending with ");
        System.out.println(anotherTestingResult);
    }

    public void binaryOperatorTest() {
        BinaryOperator<Integer> operator0 = new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer integer, Integer integer2) {
                return integer + integer2;
            }
        };
        BinaryOperator<Integer> operator1 = (a, b) -> a + b;

        System.out.println(operator1.apply(23, 32));

        /**
         * @param <T> the type of the input to the function
         * @param <R> the type of the result of the function
         */
        Function<Integer, Integer> multiplyBy2    = new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer integer) {
                return integer * 2;
            }
        };

        Function<Integer, Integer> multiplyBy2Lambda = num1 -> num1 * 2;

        Integer applyResult0 = operator1.apply(20, 30);
        BiFunction<Integer, Integer, Integer> integerIntegerIntegerBiFunction
                = operator0.andThen(multiplyBy2);
        Integer applyResult1 = integerIntegerIntegerBiFunction.apply(44, 45);
    }

    public void unaryOperatorExample() {
        UnaryOperator<Integer> square0 = new UnaryOperator<Integer>() {
            @Override
            public Integer apply(Integer integer) {
                return integer * integer;
            }
        };

        UnaryOperator<Integer> square1 = x -> x*x;
        System.out.println(square1.apply(5));
    }

    public void consumerDemo() {
        new Consumer() {
            @Override
            public void accept(Object o) {
                System.out.println(o.toString());
            }
        };

        Consumer consumer = o -> System.out.println(o.toString());
        consumer.accept(12);
    }

    public void zeroOperator() {
        Runnable zero_parameter_lambda = () -> System.out.println("Zero parameter lambda");
        zero_parameter_lambda.run();
    }

    public void oneOperator() {
        Consumer<String> consumer = p -> System.out.println("One parameter: " + p);
        consumer.accept("parameter value");
    }

    public void twoOperators() {
        BiConsumer handler = (p1, p2) -> System.out.println("Multiple parameters: " + p1 + ", " + p2);
        handler.accept("first", "second");

        Comparator<Integer> tComparator = (o1, o2) -> o1 - o2;
    }

    public void streamReduceExample() {

        List<Integer> numbers = Arrays.asList(1, 2);
        Optional<Integer> reduce = numbers.stream().reduce((x, y) -> x + y);
        System.out.println("reduce((x, y) -> x + y) = " + reduce.get());

        numbers = Arrays.asList(1, 2, 3);
        reduce = numbers.stream().reduce((x, y) -> x + y);
        System.out.println("reduce((x, y) -> x + y) = " + reduce.get());

        Integer iReduce = numbers.stream().reduce(20, (x, y) -> x * y);
        System.out.println("reduce(20, (x, y) -> x * y) = " + iReduce);

        numbers = Arrays.asList(1, 2, 3, 5);

        Optional<Integer> sum = numbers.stream()
                .reduce((left, right) -> left + right);

        sum.ifPresent(System.out::println);

        Integer iSum = numbers.stream()
                .reduce(11, (left, right) -> left + right);
        System.out.println(iSum);

        // 1*10 + 2*10 + 3*10
        iSum = numbers.stream()
                .reduce(10, (identity, val) -> identity * val, (left, right) -> left + right);
        System.out.println(iSum);

        // с нашими примерами функциоральных интерфейсов
        Optional<Integer> sumResult0 = numbers.stream().reduce((integer, integer2) -> integer + integer2);
        System.out.println("(integer, integer2) -> integer + integer2 = " + sumResult0.get());

        BinaryOperator<Integer> operator1 = (a, b) -> a + b;
        Optional<Integer> sumResult1 = numbers.stream().reduce(operator1);
        System.out.println("reduce(operator1) = " + sumResult1.get());

    }

    public void minValueByStream() {
        Integer min = Arrays.asList(1, -34, 2, 25, 3, 5)
                .stream()
                .reduce(Integer.MAX_VALUE, (left, right) -> left < right ? left : right);

        System.out.println("min among 1, 2, 25, 3, 5 is " + min);
    }

    public void maxValueByStream() {
        Integer min = Arrays.asList(1, -34, 2, 25, 3, 5)
                .stream()
                .reduce(Integer.MIN_VALUE, (left, right) -> left > right ? left : right);

        System.out.println("min among 1, 2, 25, 3, 5 is " + min);
    }


    public static String printTest(Integer i) {
        System.out.println(i);
        return String.valueOf(i);
    }

    public void rangeExample() {
        List<String> collect = IntStream.rangeClosed(2, 7)
                .filter(i -> i % 2 == 0)
                .mapToObj(String::valueOf)
                .collect(Collectors.toList());
        collect.forEach(System.out::println);
    }

    public void iterateByStream() {
        Stream.iterate("*",s  -> {
            if (s.length() > 5) {
                s = s.substring(0, s.length() - 1);
            } else  {
                s = s.concat("*");
            }
            return s;
        }).limit(10)
          .forEach(System.out::println);
    }
}
