package lesson18.jdbc.isolation;

import org.apache.derby.jdbc.EmbeddedDataSource;

import java.sql.*;

/**
 *
 *    READ_UNCOMMITTED
 * -> READ_COMMITTED (Unrepeatable Read)
 *    REPEATABLE_READ
 *    SERIALIZABLE
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-27
 */
public class ReadCommitted {

    private static final String CREATE_PERSONS_TABLE = "CREATE TABLE Persons " +
                                                           "(ID int not null primary key, " +
                                                           "NAME varchar(255), " +
                                                           "AGE int" +
                                                       ")";
    private static final String DROP_PERSONS_TABLE = "DROP TABLE Persons";
    private static final String INSERT_SQL = "INSERT INTO Persons VALUES (1, 'Иван', 30)";
    private static final String SELECT_SQL = "SELECT * FROM Persons WHERE ID = 1";
    private static final String UPDATE_SQL = "UPDATE Persons SET AGE = 31 WHERE ID = 1";

    private static final String AGE_COLUMN = "AGE";


    private EmbeddedDataSource ds;

    public ReadCommitted() throws SQLException {
        ds = new EmbeddedDataSource();
        ds.setDatabaseName("Derby");
        ds.setCreateDatabase("create");
        Connection conn = ds.getConnection();

        PreparedStatement stmt = conn.prepareStatement(CREATE_PERSONS_TABLE);
        stmt.execute();
        stmt.close();

        stmt = conn.prepareStatement(INSERT_SQL);
        stmt.execute();
        stmt.close();
        conn.close();
    }

    private void dropTable() throws SQLException {
        Connection conn = ds.getConnection();
        PreparedStatement stmt = conn.prepareStatement(DROP_PERSONS_TABLE);
        stmt.execute();
        stmt.close();
    }

    /**
     * "Неповторимое чтение":
     * 1) 1-ая транзакция выполняет 1-ый запрос на получение AGE
     * 2) 2-ая транзакция обновляет AGE с commit
     * 3) 1-ая транзакция выполняет 2-ой запрос на получение AGE
     *
     * Итог: различные данные в обоих запросах в рамках 1-ой транзакции
     *
     * @throws SQLException
     * @throws InterruptedException
     */
    public void tryUnrepeatableRead() throws InterruptedException {
        int age_1, age_2;

        try (Connection conn = ds.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SELECT_SQL)) {

            conn.setAutoCommit(false);

            //изменение уровня изоляции транзакции
            //conn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
            conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            //conn.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
            //conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

            try (ResultSet rs = stmt.executeQuery()) {
                rs.next();
                age_1 = rs.getInt(AGE_COLUMN);
            }

            new Thread(this::updateAgeWithCommit).start();
            Thread.sleep(2000);

            try (ResultSet rs = stmt.executeQuery()) {
                rs.next();
                age_2 = rs.getInt(AGE_COLUMN);
            }

            if (age_1 != age_2)
                System.out.println("Unrepeatable Read");
            else
                System.out.println("Ok");

            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Обновление AGE с последующим commit
     *
     * @throws SQLException
     * @throws InterruptedException
     */
    private void updateAgeWithCommit() {
        try (Connection conn = ds.getConnection();
             PreparedStatement stmt = conn.prepareStatement(UPDATE_SQL)) {
            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException, InterruptedException {
        ReadCommitted obj = new ReadCommitted();
        obj.tryUnrepeatableRead();
        obj.dropTable();
    }
}
