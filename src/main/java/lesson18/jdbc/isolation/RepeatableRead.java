package lesson18.jdbc.isolation;

import org.apache.derby.jdbc.EmbeddedDataSource;

import java.sql.*;

/**
 *
 *    READ_UNCOMMITTED
 *    READ_COMMITTED
 * -> REPEATABLE_READ (Phantom Read)
 *    SERIALIZABLE
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-27
 */
public class RepeatableRead {

    private static final String CREATE_PERSONS_TABLE = "CREATE TABLE Persons " +
                                                           "(ID int not null primary key, " +
                                                           "NAME varchar(255), " +
                                                           "AGE int" +
                                                       ")";
    private static final String DROP_PERSONS_TABLE = "DROP TABLE Persons";
    private static final String INSERT_SQL = "INSERT INTO Persons VALUES (1, 'Иван', 30)";
    private static final String INSERT_OTHER_SQL = "INSERT INTO Persons VALUES (2, 'Петр', 40)";
    private static final String SELECT_COUNT_ROWS_SQL = "SELECT COUNT(*) FROM Persons";


    private EmbeddedDataSource ds;

    public RepeatableRead() throws SQLException {
        ds = new EmbeddedDataSource();
        ds.setDatabaseName("Derby");
        ds.setCreateDatabase("create");
        Connection conn = ds.getConnection();

        PreparedStatement stmt = conn.prepareStatement(CREATE_PERSONS_TABLE);
        stmt.execute();
        stmt.close();

        stmt = conn.prepareStatement(INSERT_SQL);
        stmt.execute();
        stmt.close();
        conn.close();
    }

    private void dropTable() throws SQLException {
        Connection conn = ds.getConnection();
        PreparedStatement stmt = conn.prepareStatement(DROP_PERSONS_TABLE);
        stmt.execute();
        stmt.close();
    }

    /**
     * "Фантомное чтение":
     * 1) 1-ая транзакция выполняет 1-ый запрос на получение количества записей
     * 2) 2-ая транзакция добавляет новую запись с commit
     * 3) 1-ая транзакция выполняет 2-ой запрос на получение количества записей
     *
     * Итог: различное количество записей в обоих запросах в рамках 1-ой транзакции
     *
     * @throws SQLException
     * @throws InterruptedException
     */
    public void tryPhantomRead() throws InterruptedException {
        int rowCount_1, rowCount_2;

        try (Connection conn = ds.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SELECT_COUNT_ROWS_SQL)) {

            conn.setAutoCommit(false);

            //изменение уровня изоляции транзакции
            //conn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
            //conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            conn.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
            //conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

            try (ResultSet rs = stmt.executeQuery()) {
                rs.next();
                rowCount_1 = rs.getInt(1);
            }

            new Thread(this::insertOtherPerson).start();
            Thread.sleep(2000);

            try (ResultSet rs = stmt.executeQuery()) {
                rs.next();
                rowCount_2 = rs.getInt(1);
            }

            if (rowCount_1 != rowCount_2)
                System.out.println("Phantom Read");
            else
                System.out.println("Ok");

            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Добавление новой записи
     */
    private void insertOtherPerson() {
        try (Connection conn = ds.getConnection();
             PreparedStatement stmt = conn.prepareStatement(INSERT_OTHER_SQL)) {
            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException, InterruptedException {
        RepeatableRead obj = new RepeatableRead();
        obj.tryPhantomRead();
        obj.dropTable();
    }
}
