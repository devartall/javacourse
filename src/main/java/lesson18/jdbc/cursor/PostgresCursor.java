package lesson18.jdbc.cursor;

import java.sql.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-29
 */
public class PostgresCursor {

    private static final String JDBC_DRIVER = "org.postgresql.Driver";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

    private static final String USER = "postgres";
    private static final String PASS = "admin";

    private static final String SELECT_SQL = "SELECT * FROM Persons";

    public static void withoutCursor() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER,PASS)) {
            Statement stmt = conn.createStatement();
            System.out.println(stmt.getQueryTimeout());
            System.out.println("m: " + stmt.getMaxRows());
            executeQuery(stmt);
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Для открытия cursor-а необходимо:
     * - установить autoCommit как false
     * - установить fetchSize больше 0
     */
    public static void withCursor() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER,PASS)) {
            conn.setAutoCommit(false);

            Statement stmt = conn.createStatement();
            stmt.setFetchSize(1);

            executeQuery(stmt);

            conn.commit();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    private static void executeQuery(Statement stmt) throws SQLException {
        long beforeTime = System.currentTimeMillis();
        ResultSet rs = stmt.executeQuery(SELECT_SQL);

        while (rs.next()) {
            System.out.println(rs.getInt(1));
            rs.getInt(1);
        }
        long afterTime = System.currentTimeMillis();

        System.out.println((afterTime - beforeTime) + " ms");
    }

    public static void main(String[] args) {
        withoutCursor();
        //withCursor();
    }
}
