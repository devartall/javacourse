package lesson18.jdbc.resultSet;

import java.sql.*;

/**
 *  TYPE_FORWARD_ONLY - указатель двигается только вперед (DEFAULT);
 *  TYPE_SCROLL_INTENSIVE - указатель может двигаться вперед/назад;
 *  TYPE_SCROLL_SENSITIVE - указатель может двигаться вперед/назад
 *                          и чувствителен к изменениям в БД, которые
 *                          произошли после создания экземпляра ResultSet
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-28
 */
public class ResultSetDemo {

    private static final String JDBC_DRIVER = "org.postgresql.Driver";

    //dataSource
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String USER = "postgres";
    private static final String PASS = "admin";

    //sql
    private static final String SELECT_SQL = "SELECT * FROM Persons";

    public void select() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
             Statement stmt = conn.createStatement()) {
            System.out.println("Person selected:");
            stmt.execute(SELECT_SQL);

            ResultSet rs = stmt.getResultSet();
            StringBuilder sb = new StringBuilder();
            while (rs.next()) {
                sb.append("{id = ");
                sb.append(rs.getInt(1));
                sb.append(", name = ");
                sb.append(rs.getString(2));
                sb.append(", age = ");
                sb.append(rs.getInt(3));
                sb.append("}\n");
            }

            System.out.println(sb);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
             Statement stmt = conn.createStatement(
                 ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)) {

            conn.setAutoCommit(false);
            stmt.execute(SELECT_SQL);

            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                if (rs.getInt(1) == 3) {
                    rs.updateInt(3, 100);
                }
                rs.updateRow();
            }
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
             Statement stmt = conn.createStatement(
                 ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {

            conn.setAutoCommit(false);

            stmt.execute(SELECT_SQL);

            ResultSet rs = stmt.getResultSet();
            rs.moveToInsertRow();
            rs.updateInt(1, 200);
            rs.updateString(2, "Петр");
            rs.updateInt(3, 50);
            rs.insertRow();

            rs.moveToCurrentRow();
            while (rs.relative(2)) {
                System.out.println("ID = " + rs.getInt(1));
            }
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ResultSetDemo obj = new ResultSetDemo();

        /*obj.select();
        obj.update();
        System.out.println("-----------");
        obj.select();
        System.out.println("-----------");
        obj.insert();
        obj.select();*/
    }
}
