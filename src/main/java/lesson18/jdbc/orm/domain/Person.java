package lesson18.jdbc.orm.domain;

import lesson18.jdbc.orm.annotations.Attribute;
import lesson18.jdbc.orm.annotations.Domain;
import lesson18.jdbc.orm.annotations.ID;
import lesson18.jdbc.orm.annotations.Sequence;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-31
 */
@Domain(table = "persons")
@Sequence(name = "persons_id_seq")
public class Person {

    @ID(name = "id")
    private Long id;

    @Attribute(name = "name")
    private String name;

    @Attribute(name = "age")
    private Integer age;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person {" +
                   "id='" + id + "\', " +
                   "name='" + name + "\', " +
                   "age=" + age +
               "}";
    }
}
