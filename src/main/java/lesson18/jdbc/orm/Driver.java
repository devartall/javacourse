package lesson18.jdbc.orm;

import lesson18.jdbc.orm.domain.Person;
import lesson18.jdbc.orm.service.PersonService;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-31
 */
public class Driver {

    public static void main(String[] args) {
        PersonService service = new PersonService();

        Person person_1 = service.findById(1L);

        System.out.println(person_1);

        Person person_2 = new Person();
        person_2.setName("Василий");
        person_2.setAge(20);

        System.out.println("Before save ID = " + person_2.getId());
        service.save(person_2);
        System.out.println("After save ID = " + person_2.getId());
    }
}
