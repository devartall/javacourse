package lesson18.jdbc.orm.service;

import lesson18.jdbc.orm.datamapper.PersonMapper;
import lesson18.jdbc.orm.domain.Person;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-31
 */
public class PersonService {

    private PersonMapper mapper = new PersonMapper();

    public Person findById(Long id) {
        return mapper.findById(id);
    }

    public int save(Person person) {
        return mapper.save(person);
    }
}
