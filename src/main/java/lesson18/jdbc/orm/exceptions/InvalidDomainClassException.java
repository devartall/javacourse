package lesson18.jdbc.orm.exceptions;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-31
 */
public class InvalidDomainClassException extends RuntimeException {

    public InvalidDomainClassException(String message) {
        super();
    }
}
