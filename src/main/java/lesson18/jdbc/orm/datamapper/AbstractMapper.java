package lesson18.jdbc.orm.datamapper;

import lesson18.jdbc.orm.annotations.Attribute;
import lesson18.jdbc.orm.annotations.Domain;
import lesson18.jdbc.orm.annotations.ID;
import lesson18.jdbc.orm.annotations.Sequence;
import lesson18.jdbc.orm.datasource.Database;
import lesson18.jdbc.orm.exceptions.InvalidDomainClassException;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-31
 */
public abstract class AbstractMapper<T> {

    private static final String NOT_DOMAIN_MESSAGE = "Class is not domain. Add an annotation @Domain to the class declaration";
    private static final String NOT_ID_MESSAGE = "Class does not have ID field. Add an annotation @ID to some field declaration";

    //identity map
    private Map<Object, T> loadedMap = new HashMap<>();

    private Map<Class, String> selectByIdQueries = new HashMap<>();
    private Map<Class, String> insertQueries = new HashMap<>();
    private Map<Class, ArrayDeque<String>> fields = new HashMap<>();

    public T findById(Object id) {
        T result = loadedMap.get(id);
        if (result != null) return result;

        String sql = selectByIdQueries.get(this.getClass());

        if (sql == null) {
            sql = generate_SELECT_BY_ID_query();
            selectByIdQueries.put(this.getClass(), sql);
        }

        try (PreparedStatement stmt = Database.prepareStatement(sql)) {
            stmt.setObject(1, id);

            ResultSet rs = stmt.executeQuery();
            rs.next();
            return load(id, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String generate_SELECT_BY_ID_query() {
        Type[] actualTypeArguments = ((ParameterizedType) (this.getClass().getGenericSuperclass())).getActualTypeArguments();
        Class<?> clazz = (Class) actualTypeArguments[0];

        Domain domain = clazz.getAnnotation(Domain.class);

        if (domain == null)
            throw new InvalidDomainClassException(NOT_DOMAIN_MESSAGE);

        StringBuilder sb = new StringBuilder("SELECT ");

        Field ID = null;
        for (Field field : clazz.getDeclaredFields()) {
            Attribute attribute = field.getAnnotation(Attribute.class);
            if (attribute != null) {
                sb.append(attribute.name()).append(", ");
            }

            ID id = field.getAnnotation(ID.class);
            if (id != null) {
                sb.append(id.name()).append(", ");
                ID = field;
            }
        }

        if (ID == null)
            throw new InvalidDomainClassException(NOT_ID_MESSAGE);

        sb.deleteCharAt(sb.length() - 1);
        sb.deleteCharAt(sb.length() - 1);

        sb.append(" FROM ");
        sb.append(domain.table());
        sb.append(" WHERE ");
        sb.append(ID.getName());
        sb.append(" = ?");

        return sb.toString();
    }


    private T load(Object id, ResultSet rs) throws SQLException {
        Type[] actualTypeArguments = ((ParameterizedType) (this.getClass().getGenericSuperclass())).getActualTypeArguments();
        Class clazz = (Class) actualTypeArguments[0];

        Object domain;

        try {
            domain = clazz.newInstance();

            for (Field field : clazz.getDeclaredFields()) {
                if (field.isAnnotationPresent(ID.class)) {
                    field.setAccessible(true);
                    field.set(domain, id);
                } else {
                    Attribute attribute = field.getAnnotation(Attribute.class);
                    if (attribute != null) {
                        field.setAccessible(true);
                        field.set(domain, rs.getObject(attribute.name()));
                    }
                }
            }

            loadedMap.put(id, (T)domain);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return loadedMap.get(id);
    }

    public int save(T domain) {
        String sql = insertQueries.get(this.getClass());

        if (sql == null) {
            sql = generate_INSERT_query(domain);
            insertQueries.put(this.getClass(), sql);
        }

        try (PreparedStatement stmt = Database.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            Class<?> clazz = domain.getClass();

            int i = 1;
            for (String fieldName : fields.get(clazz)) {
                Field field = clazz.getDeclaredField(fieldName);
                field.setAccessible(true);
                stmt.setObject(i++, field.get(domain));
            }

            stmt.executeUpdate();

            for (Field field : clazz.getDeclaredFields()) {
                if (field.isAnnotationPresent(ID.class)) {

                    ResultSet keys = stmt.getGeneratedKeys();
                    keys.next();
                    field.setAccessible(true);
                    field.set(domain, keys.getObject(1));
                    break;
                }
            }
        } catch (IllegalArgumentException
               | IllegalAccessException
               | NoSuchFieldException
               | SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private String generate_INSERT_query(T entity) {
        Class<?> clazz = entity.getClass();

        Domain domain = clazz.getAnnotation(Domain.class);
        if (clazz.getAnnotation(Domain.class) == null)
            throw new InvalidDomainClassException(NOT_DOMAIN_MESSAGE);

        Sequence sequence = clazz.getAnnotation(Sequence.class);

        ArrayDeque<String> attrNames = new ArrayDeque<>();
        ArrayDeque<String> fieldNames = new ArrayDeque<>();

        try {
            for (Field field : clazz.getDeclaredFields()) {
                ID id = field.getAnnotation(ID.class);
                if (id != null) {
                    attrNames.addFirst(id.name());
                } else {
                    Attribute attribute = field.getAnnotation(Attribute.class);
                    if (attribute != null) {
                        attrNames.add(attribute.name());
                        fieldNames.add(field.getName());
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        fields.put(clazz, fieldNames);

        StringBuilder sb = new StringBuilder("INSERT INTO ");
        sb.append(domain.table());
        sb.append("(");

        for (String attr : attrNames) {
            sb.append(attr);
            sb.append(",");
        }

        sb.deleteCharAt(sb.length() - 1);
        sb.append(")");

        sb.append(" VALUES(nextval('");
        sb.append(sequence.name());
        sb.append("'),");

        for (int i = 0; i < fieldNames.size(); i++) {
            sb.append("?,");
        }

        sb.deleteCharAt(sb.length() - 1);
        sb.append(")");

        return sb.toString();
    }
}
