package lesson18.jdbc.orm.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-31
 */
@Target(value={FIELD})
@Retention(value=RUNTIME)
public @interface Attribute {

    String name() default "";
}
