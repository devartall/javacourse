package lesson18.jdbc.orm.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-31
 */
@Target(value={TYPE})
@Retention(value=RUNTIME)
public @interface Domain {

    String table() default "";
}
