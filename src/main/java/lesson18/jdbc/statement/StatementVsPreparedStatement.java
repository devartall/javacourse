package lesson18.jdbc.statement;

import java.sql.*;
import java.util.Random;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-29
 */
public class StatementVsPreparedStatement {

    private static Random random = new Random();

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

    private static final String USER = "postgres";
    private static final String PASS = "admin";

    /**
     * Тестируем Statement
     */
    public static void executeStatement() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {
            try (Statement stmt = conn.createStatement()) {

                for (int i = 0; i < 2_000; i++) {
                    int id = random.nextInt(1000) + 1100;

                    //insert
                    stmt.executeUpdate("INSERT INTO Persons VALUES(" + id + ", 'Иван', 35)");

                    //delete
                    stmt.executeUpdate("DELETE FROM Persons WHERE ID = " + id);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void testStatement() {
        int iters = 10;
        long sum = 0;
        for (int i = 0; i < iters; i++) {
            long before = System.currentTimeMillis();
            executeStatement();
            long after = System.currentTimeMillis();
            sum += (after - before);
        }
        System.out.println(sum / iters + " ms");
    }

    /**
     * Тестируем PreparedStatement
     */
    public static void executePreparedStatement() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {
            try (PreparedStatement INSERT_QUERY =
                     conn.prepareStatement("INSERT INTO Persons VALUES(?, 'Иван', 35)");
                 PreparedStatement DELETE_QUERY =
                     conn.prepareStatement("DELETE FROM Persons WHERE ID = ?")) {

                for (int i = 0; i < 2_000; i++) {
                    int id = random.nextInt(1000) + 1100;

                    INSERT_QUERY.setInt(1, id);
                    INSERT_QUERY.executeUpdate();

                    DELETE_QUERY.setInt(1, id);
                    DELETE_QUERY.executeUpdate();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void testPreparedStatement() {
        int iters = 10;
        long sum = 0;
        for (int i = 0; i < iters; i++) {
            long before = System.currentTimeMillis();
            executePreparedStatement();
            long after = System.currentTimeMillis();
            sum += (after - before);
        }
        System.out.println(sum / iters + " ms");
    }

    public static void main(String[] args) {
        //testStatement();
        testPreparedStatement();
    }
}
