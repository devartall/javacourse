package lesson18.jdbc.statement;

import java.sql.*;
import java.util.Random;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-30
 */
public class StatementAPI {

    private static Random random = new Random();

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

    private static final String USER = "postgres";
    private static final String PASS = "admin";

    public static void updateCount_example() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate("UPDATE Persons SET AGE = AGE + 10 WHERE NAME = 'Иван'");

                System.out.println("UpdateCount: " + stmt.getUpdateCount());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void withoutBatch_example() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {
            try (Statement stmt = conn.createStatement()) {

                for (int i = 0; i < 2_000; i++) {
                    int id = random.nextInt(1000) + 2000;

                    stmt.executeUpdate("INSERT INTO Persons VALUES(" + id + ", 'Иван', 35)");
                    stmt.executeUpdate("DELETE FROM Persons WHERE ID = " + id);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void testWithoutBatch() {
        int iters = 10;
        int sum = 0;
        for (int i = 0; i < iters; i++) {
            long before = System.currentTimeMillis();
            withoutBatch_example();
            long after = System.currentTimeMillis();
            sum += (after - before);
        }
        System.out.println(sum / iters);
    }
    public static void batch_example() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {
            try (Statement stmt = conn.createStatement()) {

                for (int i = 0; i < 2_000; i++) {
                    int id = random.nextInt(1000) + 2000;

                    stmt.addBatch("INSERT INTO Persons VALUES(" + id + ", 'Иван', 35)");
                    stmt.addBatch("DELETE FROM Persons WHERE ID = " + id);
                }

                stmt.executeBatch();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void testBatch() {
        int iters = 10;
        int sum = 0;
        for (int i = 0; i < iters; i++) {
            long before = System.currentTimeMillis();
            batch_example();
            long after = System.currentTimeMillis();
            sum += (after - before);
        }
        System.out.println(sum / iters);
    }

    public static void getMetadata() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Persons WHERE id = 1")) {
                ResultSetMetaData metaData = stmt.getMetaData();
                System.out.println(metaData.getColumnClassName(1));
                System.out.println(metaData.getColumnClassName(2));
                System.out.println(metaData.getColumnClassName(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //updateCount_example();

        //batch
        //testWithoutBatch();
        //testBatch();

        //getMetadata();
    }
}
