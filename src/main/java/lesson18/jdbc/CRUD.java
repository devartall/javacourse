package lesson18.jdbc;

import java.sql.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-28
 */
public class CRUD {

    static final String JDBC_DRIVER = "org.h2.Driver";

    //dataSource
    private static final String DB_URL = "jdbc:h2:~/test";
    private static final String USER = "sa";
    private static final String PASS = "";

    //sql
    private static final String CREATE_TABLE_SQL = "CREATE TABLE Persons (" +
                                                 "ID INTEGER NOT NULL PRIMARY KEY, " +
                                                 "NAME VARCHAR(255), " +
                                                 "AGE INTEGER" +
                                             ")";
    private static final String DROP_TABLE_SQL = "DROP TABLE Persons";

    private static final String INSERT_SQL = "INSERT INTO Persons VALUES(1, 'Иван', 30)";
    private static final String INSERT_OTHER_SQL = "INSERT INTO Persons VALUES(2, 'Петр', 40)";
    private static final String SELECT_SQL = "SELECT * FROM Persons";
    private static final String UPDATE_SQL = "UPDATE Persons SET age = 35 WHERE id = 1";
    private static final String DELETE_SQL = "DELETE FROM Persons WHERE id = 1";

    public void createTablePersons() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER,PASS);
             Statement stmt = conn.createStatement()) {
            //Class.forName(JDBC_DRIVER);

            stmt.executeUpdate(CREATE_TABLE_SQL);
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropTablePersons() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(DROP_TABLE_SQL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertPersons() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(INSERT_SQL);
            stmt.executeUpdate(INSERT_OTHER_SQL);

            System.out.println("Persons inserted.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void selectPersons() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
             PreparedStatement stmt = conn.prepareStatement(SELECT_SQL)) {

            ResultSet rs = stmt.executeQuery();

            StringBuilder sb = new StringBuilder();
            while (rs.next()) {
                sb.append("{id = ");
                sb.append(rs.getInt(1));
                sb.append(", name = ");
                sb.append(rs.getString(2));
                sb.append(", age = ");
                sb.append(rs.getInt(3));
                sb.append("}\n");
            }

            System.out.println(sb);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePerson() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(UPDATE_SQL);

            System.out.println("Persons updated.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deletePerson() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(DELETE_SQL);

            System.out.println("Person deleted.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        CRUD crud = new CRUD();
        crud.createTablePersons();
        crud.insertPersons();
        crud.selectPersons();

        crud.dropTablePersons();
    }
}
