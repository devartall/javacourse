package lesson18.jdbc;

import java.sql.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-23
 */
public class DatabaseMetaData_example {

    static final String JDBC_DRIVER = "org.h2.Driver";
    static final String DB_URL = "jdbc:h2:~/test";

    static final String USER = "sa";
    static final String PASS = "";

    public static void main(String[] args) {
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection conn = DriverManager.getConnection(DB_URL, USER,PASS)) {
            DatabaseMetaData metaData = conn.getMetaData();

            System.out.println(metaData.getDriverVersion());
            System.out.println(metaData.getDatabaseProductName());
            System.out.println(metaData.getDatabaseProductVersion());
            System.out.println(metaData.supportsFullOuterJoins());
            System.out.println(metaData.supportsStoredProcedures());
            System.out.println(metaData.supportsStatementPooling());
            System.out.println(metaData.getMaxConnections());
            System.out.println(metaData.supportsMultipleTransactions());
            System.out.println(metaData.getDefaultTransactionIsolation());
            System.out.println(metaData.getJDBCMinorVersion());
            System.out.println(metaData.getJDBCMajorVersion());
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }
}
