package lesson9;

import lesson6.Car;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.*;
import java.util.Date;

public class ReflectionExamples {

    private static final Log LOG = LogFactory.getLog(ReflectionExamples.class);

    public Integer integerVariable;

    private static class MyClass {
        private class MyInnerClass {
            int i;
        }

        MyInnerClass myInnerClass;
    }

    public static void main(String... args) {


        long startTime = new Date().getTime();
        Car car = new Car();
        for (int i = 0; i < 100000; i++) {
            car.openCar();
        }
        long endTime = new Date().getTime();
        long performanceTime = endTime - startTime;


        Class<Car> carClass = Car.class;
        try {
            startTime = new Date().getTime();
            Constructor<Car> constructor = carClass.getConstructor(null);
            Car car1 = constructor.newInstance();
            Method openCar = carClass.getMethod("openCar", null);
            for (int i = 0; i < 100000; i++) {
                openCar.invoke(car1, null);
            }
            endTime = new Date().getTime();
            long performanceTime1 = endTime - startTime;
            System.out.println("drive method direct invocation time: " + performanceTime);
            System.out.println("drive method reflection invocation time: " + performanceTime1);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        getObjectType();
        getDeclaredClassFieldsExample();
        getDeclaredClassFieldByNameExample();
        getFields();
        issue();
    }


    private static void getObjectType() {
        String str = new String();
        final Method[] declaredMethods = String.class.getDeclaredMethods();
        System.out.println(str.getClass().getName());
        System.out.println(String.class.getName());
        String nameOfClass = "java.lang.String";
        try {
            Class clazz = Class.forName(nameOfClass);
            System.out.println(clazz);
        } catch (ClassNotFoundException e) {
            System.err.println("No class with name " + nameOfClass + " found");
        }
    }


    private static void getDeclaredClassFieldsExample() {
        // можно получать информацию в том числе о внутренних классах
        Class<MyClass.MyInnerClass> c = MyClass.MyInnerClass.class;
        for (Field field : c.getDeclaredFields()) {
            LOG.info(String.format("Field name: %s, type: %s",
                                   field.getName(),
                                   field.getType().getSimpleName()));
        }

        Class anotherClass = ReflectionExamples.class;
        Field[] declaredFields = anotherClass.getDeclaredFields();
        System.out.println(declaredFields.length);
    }

    private static void getDeclaredClassFieldByNameExample() {
        try {
            Field logField = ReflectionExamples.class.getDeclaredField("LOG");
            System.out.println(logField);
            System.out.println(logField.getName());
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private static void getFields() {
        Field[] fields = String.class.getFields();
        for (Field field : fields) {
            System.out.println(field);
        }

        fields = String.class.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field);
        }
    }

    private static void issue() {
        ReflectionExamples example = new ReflectionExamples();
        try {
            Field field = ReflectionExamples.class.getField("integerVariable");
            Field type = Field.class.getDeclaredField("type");
            type.setAccessible(true);
            type.set(field, String.class);
            field.set(example, "A");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            LOG.error(e.toString());
        }
        System.out.println(example.integerVariable);

        SecurityManager securityManager = System.getSecurityManager();
        securityManager.checkPermission(new ReflectPermission("reflectPermission"));
    }

}
