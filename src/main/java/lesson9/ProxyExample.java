package lesson9;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyExample  {
    interface IOriginalType {
        void originalMethod(String s);
    }
    static class Original implements IOriginalType {
        public void originalMethod(String s) {
            System.out.println(s);
        }
    }
    static class Handler implements InvocationHandler {
        private final IOriginalType original;
        public Handler(IOriginalType original) {
            this.original = original;
        }

        public Object invoke(Object proxy, Method method, Object[] args)
                throws IllegalAccessException, IllegalArgumentException,
                InvocationTargetException {
            System.out.println("BEFORE");
            method.invoke(original, args);
            System.out.println("AFTER");
            return null;
        }
    }

    public static void main(String[] args){
        Original original = new Original();
        Handler handler = new Handler(original);
        IOriginalType proxyObject = (IOriginalType)Proxy.newProxyInstance(IOriginalType.class.getClassLoader(),
                new Class[] { IOriginalType.class },
                handler);
        proxyObject.originalMethod("String to be passed into original method");
    }
}
