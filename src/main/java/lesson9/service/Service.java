package lesson9.service;

import groovy.lang.GroovyClassLoader;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Класс для прослушивания папки с плагинами
 */
public class Service {

    public static void main(String...agrs) throws InterruptedException {
        while (true) {
            //определение текущей директории
            File file = new File("/Users/fedkapot/javacourse/plugins");
            File[] classFiles = file.listFiles();
            for (File pluginFile : classFiles) {
                try {
                    GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
                    Class aClass = groovyClassLoader.parseClass(pluginFile);
                    if (IUselessService.class.isAssignableFrom(aClass)) {
                        IUselessService newService = (IUselessService)
                                aClass.getConstructor(null).newInstance(null);
                        newService.uselessMethod();
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }

            Thread.sleep(5000L);
        }
    }
}
