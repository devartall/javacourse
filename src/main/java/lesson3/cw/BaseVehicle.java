package lesson3.cw;

import java.util.Date;

/**
 * Базовый класс, определяющий объекты множества "транспортное средство"
 */
public abstract class BaseVehicle implements IRollingStock {
    static long NUMBER_OF_INSTANCES;

    private long id;
    private int maxSpeed;
    private String model;
    private Date issueDate;
    private Dimensions dimensions;

    public BaseVehicle() {
        id = NUMBER_OF_INSTANCES++;
        issueDate = new Date();
    }

    public BaseVehicle(long id) {
        this();
        this.id = id;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }
}
