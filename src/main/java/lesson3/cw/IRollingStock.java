package lesson3.cw;

/**
 * Базовый интерфейс, который должен быть реализован классами,
 * представляющими собой подвижной состав
 */
public interface IRollingStock {
    long getId();
}
