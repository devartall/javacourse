package lesson3.cw;

public class Truck extends BaseVehicle {
    private int volume;

    @Override
    public String toString() {
        return "truck: id = " + getId() + ", volume = " + volume;
    }

    public String toString(boolean showId) {

        return showId ? toString() : "truck: volume = " + volume;

    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
