package lesson3.cw;

/**
 * Класс реализует абстракцию легкового автомобиля
 */
public class PassengerCar extends BaseVehicle {
    int numberOfSeats;

    public PassengerCar() {
        numberOfSeats = 5;
    }
}
