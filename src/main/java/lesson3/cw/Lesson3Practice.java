package lesson3.cw;

import lesson5.cw.ClassWithDifferentAttributes;

public class Lesson3Practice {
    public static void main(String [] args) {
        PassengerCar car = new PassengerCar();
        Truck truck = new Truck();
        truck.setVolume(24);
        System.out.println(BaseVehicle.NUMBER_OF_INSTANCES);
        // overload
        System.out.println(truck.toString(false));
        // override
        System.out.println(truck.toString());

        System.out.println(checkId(car));
        System.out.println(checkId(truck));

        ClassWithDifferentAttributes anotherExample = new ClassWithDifferentAttributes();
        anotherExample.apiAccess = "we can change it";
    }

    public static boolean checkId(IRollingStock rollingStock) {
        return rollingStock.getId() < BaseVehicle.NUMBER_OF_INSTANCES;
    }

}
