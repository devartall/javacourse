package lesson17.foodmachine.oop.discount;

import lesson17.foodmachine.oop.machine.Cell;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-14
 */
public interface DiscountStrategy {

    int make(Cell cell);
}
