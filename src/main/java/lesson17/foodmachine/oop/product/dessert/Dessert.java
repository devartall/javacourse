package lesson17.foodmachine.oop.product.dessert;

import lesson17.foodmachine.oop.product.Product;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public class Dessert extends Product {

    public Dessert(String title) {
        super(title);
    }
}
