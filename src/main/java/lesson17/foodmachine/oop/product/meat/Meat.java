package lesson17.foodmachine.oop.product.meat;

import lesson17.foodmachine.oop.product.Product;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public class Meat extends Product {

    public Meat(String title) {
        super(title);
    }
}
