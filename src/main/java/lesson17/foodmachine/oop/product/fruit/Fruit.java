package lesson17.foodmachine.oop.product.fruit;

import lesson17.foodmachine.oop.product.Product;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public class Fruit extends Product {

    public Fruit(String title) {
        super(title);
    }
}
