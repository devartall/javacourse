package lesson17.foodmachine.oop.product.shake;

import lesson17.foodmachine.oop.product.Product;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public class Shake extends Product {

    public Shake(String title) {
        super(title);
    }
}
