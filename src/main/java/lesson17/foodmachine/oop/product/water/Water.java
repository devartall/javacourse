package lesson17.foodmachine.oop.product.water;

import lesson17.foodmachine.oop.product.Product;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public class Water extends Product {

    public Water(String title) {
        super(title);
    }
}
