package lesson17.foodmachine.oop.product.bar;

import lesson17.foodmachine.oop.product.Product;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public abstract class Bar extends Product {

    public Bar(String title) {
        super(title);
    }
}
