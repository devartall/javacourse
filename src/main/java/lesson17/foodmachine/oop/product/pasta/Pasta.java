package lesson17.foodmachine.oop.product.pasta;

import lesson17.foodmachine.oop.product.Product;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public class Pasta extends Product {

    public Pasta(String title) {
        super(title);
    }
}
