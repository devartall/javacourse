package lesson17.foodmachine.oop.product.porridge;

import lesson17.foodmachine.oop.product.Product;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public class Porridge extends Product {

    public Porridge(String title) {
        super(title);
    }
}
