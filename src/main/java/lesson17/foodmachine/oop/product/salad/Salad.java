package lesson17.foodmachine.oop.product.salad;

import lesson17.foodmachine.oop.product.Product;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public abstract class Salad extends Product {

    public Salad(String title) {
        super(title);
    }
}
