package lesson17.foodmachine.oop.factory.product;

import lesson17.foodmachine.oop.product.bar.Bar;
import lesson17.foodmachine.oop.product.bar.ChocolateBar;
import lesson17.foodmachine.oop.product.dessert.Dessert;
import lesson17.foodmachine.oop.product.dessert.Doughnut;
import lesson17.foodmachine.oop.product.fruit.Fruit;
import lesson17.foodmachine.oop.product.fruit.StrawberryWithCream;
import lesson17.foodmachine.oop.product.meat.Cutlet;
import lesson17.foodmachine.oop.product.meat.Meat;
import lesson17.foodmachine.oop.product.pasta.Carbonara;
import lesson17.foodmachine.oop.product.pasta.Pasta;
import lesson17.foodmachine.oop.product.porridge.Porridge;
import lesson17.foodmachine.oop.product.porridge.PorridgeWithJam;
import lesson17.foodmachine.oop.product.salad.DressedHerring;
import lesson17.foodmachine.oop.product.salad.Salad;
import lesson17.foodmachine.oop.product.shake.MilkShake;
import lesson17.foodmachine.oop.product.shake.Shake;
import lesson17.foodmachine.oop.product.water.SodaWater;
import lesson17.foodmachine.oop.product.water.Water;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public class GeneralProductFactory implements ProductFactory {


    /*---------------------------SINGLETON---------------------------*/


    private static GeneralProductFactory factory = new GeneralProductFactory();

    private GeneralProductFactory() {}

    public static GeneralProductFactory getInstance() {
        return factory;
    }


    /*---------------------------ABSTRACT FACTORY---------------------------*/


    @Override
    public Water createWater() {
        return new SodaWater();
    }

    @Override
    public Bar createBar() {
        return new ChocolateBar();
    }

    @Override
    public Salad createSalad() {
        return new DressedHerring();
    }

    @Override
    public Fruit createFruit() {
        return new StrawberryWithCream();
    }

    @Override
    public Dessert createDessert() {
        return new Doughnut();
    }

    @Override
    public Porridge createPorridge() {
        return new PorridgeWithJam();
    }

    @Override
    public Meat createMeat() {
        return new Cutlet();
    }

    @Override
    public Pasta createPasta() {
        return new Carbonara();
    }

    @Override
    public Shake createShake() {
        return new MilkShake();
    }
}
