package lesson17.foodmachine.oop.factory;

import lesson17.foodmachine.oop.machine.FoodMachine;
import lesson17.foodmachine.oop.factory.product.ProductFactory;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public abstract class FoodMachineFactory {


    /*---------------------------FACTORY METHOD---------------------------*/


    protected ProductFactory productFactory;

    public FoodMachineFactory(ProductFactory productFactory) {
        this.productFactory = productFactory;
    }

    public abstract FoodMachine createFoodMachine();
}
