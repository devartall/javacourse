package lesson17.foodmachine.oop;

import lesson17.foodmachine.oop.factory.FitnessFoodMachineFactory;
import lesson17.foodmachine.oop.factory.FoodMachineFactory;
import lesson17.foodmachine.oop.machine.FoodMachine;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-10
 */
public class Driver {

    public static void main(String[] args) {
        FoodMachineFactory fitnessFactory = new FitnessFoodMachineFactory();
        FoodMachine foodMachine = fitnessFactory.createFoodMachine();


    }
}
