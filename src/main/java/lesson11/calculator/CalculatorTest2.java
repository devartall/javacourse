package lesson11.calculator;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-27
 */
public class CalculatorTest2 {

    private int errors = 0;

    public void testAdd() {
        Calculator calculator = new Calculator();
        double result = calculator.add(10, 20);

        if (result != 30)
            throw new IllegalStateException("Incorrect result: " + result);
    }

    public static void main(String[] args) {
        CalculatorTest2 test = new CalculatorTest2();
        try {
            test.testAdd();
        } catch (Throwable e) {
            test.errors++;
            e.printStackTrace();
        }

        if (test.errors > 0)
            throw new IllegalStateException("Errors count: " + test.errors);
    }
}
