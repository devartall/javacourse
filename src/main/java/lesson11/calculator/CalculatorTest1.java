package lesson11.calculator;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-27
 */
public class CalculatorTest1 {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        double result = calculator.add(10, 20);

        if (result != 30)
            System.out.println("Incorrect result: " + result);
    }
}
