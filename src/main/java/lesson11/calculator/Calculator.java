package lesson11.calculator;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-26
 */
public class Calculator {

    public double add(double number1, double number2) {
        return number1 + number2;
    }
}
