package lesson11.shop;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-08
 */
public class Customer {

    private BankCard bankCard;

    private Shop shop;
    private Cart cart;

    public Customer(BankCard bankCard, Shop shop) {
        this.bankCard = bankCard;
        this.shop = shop;
    }

    public void takeCart() {
        this.cart = shop.removeCart();
    }

    public void takeGood(Good good, int quantity) throws Exception {
        if (cart == null)
            throw new Exception("Некуда класть");

        int count = shop.removeGood(good, quantity);
        for (int i = 0; i < count; i++) {
            cart.addGood(good);
        }
    }

    public void pay() {
        int sum = shop.calculateSum(cart.clear());
        shop.pay(bankCard, sum);
    }
}
