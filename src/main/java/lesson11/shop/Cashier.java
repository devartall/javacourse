package lesson11.shop;

import java.util.List;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-09
 */
public class Cashier {

    public int calculateSum(List<Good> goods) {
        int amount = 0;
        for (Good good : goods)
            amount += good.getPrice();

        return amount;
    }
}
