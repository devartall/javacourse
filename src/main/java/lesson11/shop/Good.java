package lesson11.shop;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-08
 */
public enum Good {

    BREAD(50),
    MILK(100),
    MEAT(200);

    private int price;

    Good(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
