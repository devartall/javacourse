package lesson11.shop;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-08
 */
public class Cart {

    public static final int DEFAULT_CAPACITY = 10;

    private int capacity;

    private List<Good> goods;

    public Cart() {
        this.capacity = DEFAULT_CAPACITY;
        this.goods = new ArrayList<>(DEFAULT_CAPACITY);
    }

    public Cart(int capacity) {
        this.capacity = capacity;
        this.goods = new ArrayList<>(this.capacity);
    }

    public void addGood(Good good) {
        goods.add(good);
    }

    public List<Good> clear() {
        List<Good> aux = new ArrayList<>(goods);
        this.goods.clear();

        return aux;
    }
}
