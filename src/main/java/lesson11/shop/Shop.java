package lesson11.shop;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-08
 */
public class Shop {

    public static final int MAX_CARTS = 100;

    private Cashier cashier;

    private Map<Good, Integer> goods;
    //private Deque<Cart> carts = new ArrayDeque<>();
    private List<Cart> carts;

    public Shop() {
        this.cashier = new Cashier();
        this.goods = new EnumMap<>(Good.class);
        this.carts = new ArrayList<>(MAX_CARTS);
        for (int i = 0; i < MAX_CARTS; i++)
            carts.add(new Cart());
    }

    public void addGood(Good good, int count) {
        goods.compute(good, (key, value) -> value != null ? value + count : count);
    }

    public int removeGood(Good good, int quantity) {
        int result;

        int count = goods.get(good);

        if (count <= quantity) {
            goods.remove(good);
            result = count;
        } else {
            goods.replace(good, count - quantity);
            result = quantity;
        }

        return result;
    }

    public int countOf(Good good) {
        return goods.getOrDefault(good, 0);
    }

    public void addCart(Cart cart) {
        if (carts.size() < MAX_CARTS)
            carts.add(cart);
        else
            throw new RuntimeException("Превышено допустимое количество корзин");
    }

    public Cart removeCart() {
        return carts.size() > 0 ? carts.remove(carts.size() - 1) : null;
    }

    public int cartsSize() {
        return carts.size();
    }

    public int calculateSum(List<Good> goods) {
        return cashier.calculateSum(goods);
    }

    public void pay(BankCard card, Integer sum) {
        System.out.println("Shop.pay()");
    }
}
