package lesson12.hw;

/**
 * Необходимо дописать игру так, что была возможность сохранить результат на диск
 * Также для прохождения сценария все действия должны работать с помощью аннотаций
 * Подробные инструкции смотрите в комментариях
 */
public class Game {
    //Путь где лежит сохранение игрока, должно быть внутри пакета lesson12
    public static final String PATH = "";
    private Player player;

    public static void main(String[] args) {
        Game game = new Game();

        game.initPlayer(args[0]);

        Scenario scenario = new Scenario();
        scenario.loadMap(new GameMap());
        scenario.playScenario();
    }

    //Иницировать игрока. Либо нового либо загруженного из файла
    private void initPlayer(String gameParam) {
        if (gameParam.equalsIgnoreCase("-n")) {
            System.out.println("Введите имя нового игрока");
            player = new Player();

            //Ваш код

            System.out.println("Создан игрок с именем - " + player.getName());
        }
        if (gameParam.equalsIgnoreCase("-c")) {
            System.out.println("Загрузка игрока");

            //Ваш код

            System.out.println("Загружен игрок с именем " + player.getName());
        }
    }

}
