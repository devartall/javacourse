package lesson12.hw;

//Каждая карта должна быть помечена аннотацией с возможностю указать автора карты в аннотацие.
//Методы которые будут использоваться в сценарии должны быть помечены аннотациями @Move(перемещение) или @Wait(ожидание)
//Количество методов помеченными аннотацией должно быть больше 2-х
import java.util.Map;

public class GameMap {
    private int size;
    //Каждый ключ это определенная позиция в карте, в которй встречается определенное событие
    //Размер ключа =< size
    private Map<Integer, MapEvent> events;


    public MapEvent go() {
        return null;
    }

    public void recoverHealth() {
    }
}
