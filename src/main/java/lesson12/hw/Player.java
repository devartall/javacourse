package lesson12.hw;

//Сделать возможность сериализовать данный класс, поле secret не должно сериализовываться
public class Player {

    private String name;
    private int level;
    private int secret;

    public Player() {
        this.name = "Unknown";
    }

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
