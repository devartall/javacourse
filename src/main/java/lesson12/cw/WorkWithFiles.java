package lesson12.cw;

import java.io.*;

public class WorkWithFiles {

    public static final String PATH = "src\\main\\java\\lesson12\\cw\\resources\\";

    public static void main(String[] args) {
//        File file = new File(PATH + "test.txt");
//        File out = new File(PATH + "out.txt");
//
//        try (InputStream inputStream = new FileInputStream(file);
//             OutputStream outputStream = new FileOutputStream(out)) {
//            byte[] buf = new byte[1024];
//            int bytesRead;
//            while ((bytesRead = inputStream.read(buf)) > 0) {
//                outputStream.write(buf, 0, bytesRead);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        //serialize();

        deserialize();
    }

//    private static void serialize() {
//        Person person_1 = new Person("Vas'a", 1234);
//        //Сериализация
//        try (FileOutputStream fos = new FileOutputStream(PATH + "person.ser");
//             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
//            oos.writeObject(person_1);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println(person_1);
//    }

    private static void deserialize() {
        //Десериализация
        Person person_2 = null;
        try (FileInputStream fis = new FileInputStream(PATH + "person.ser");
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            person_2 = (Person) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(person_2);
    }
}