package lesson8.cw.exam;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class Teacher implements Runnable {
    private Table table = new Table();
    private Random random = new Random();
    private ConcurrentHashMap<String, ExamTask> theoryTasks = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, ExamTask> practicalTasks = new ConcurrentHashMap<>();

    public synchronized void checkPracticalTask(ExamTask practicalTask) throws InterruptedException {
        practicalTask.increaseAttempt();
        String name = Thread.currentThread().getName();
        System.out.println("Учитель проверяет практику у " + name);
        Thread.sleep(1500);
        practicalTask.checkTask();
        if (practicalTask.isNotSolved()) return;

        System.out.println(name + " сдал практику за " + practicalTask.getAttemptCounter() + " попыток");

        synchronized (table) {
            table.wait();
            getRecommendation();
        }
    }

    private void getRecommendation() throws InterruptedException {
        System.out.println("Учитель дает рекомендацию " + Thread.currentThread().getName());
        Thread.sleep(500);
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2500);
            System.out.println("Учитель пришел на экзамен");
            synchronized (this) {
                notifyAll();
            }
            boolean theoryTasksNotSolved = true;
            while (theoryTasksNotSolved) {
                Thread.sleep(1000);
                theoryTasksNotSolved = notSolvedYet(theoryTasks);
            }
            synchronized (this) {
                System.out.println("Учитель: Теоритическая часть закончена");
                notifyAll();
            }

            boolean practicalTasksNotSolved = true;
            while (practicalTasksNotSolved) {
                Thread.sleep(1000);
                practicalTasksNotSolved = notSolvedYet(practicalTasks);
                synchronized (table) {
                    table.notify();
                }
            }

            System.out.println("Учитель заполняет ведомость");
            Thread.sleep(6000);
            System.out.println("Учитель заполнил ведомость и завершил экзамен");
        } catch (InterruptedException e) {
            System.out.println("Учитель покинул экзамен");
        }
    }

    private boolean notSolvedYet(ConcurrentHashMap<String, ExamTask> tasks) {
        if (tasks.size() > 0) {
            for (ExamTask task : tasks.values()) {
                if (task.isNotSolved()) return true;
            }
            return false;
        }
        return true;
    }

    public ExamTask getTheoryTask(String name) {
        ExamTask examTask = new ExamTask(random.nextInt(2) + 1);
        theoryTasks.put(name, examTask);
        return examTask;
    }

    public synchronized void checkTheoryTask(ExamTask theoryTask) throws InterruptedException {
        theoryTask.increaseAttempt();
        String name = Thread.currentThread().getName();
        System.out.println("Учитель проверяет теорию у " + name);
        Thread.sleep(1000);
        theoryTask.checkTask();
        if (theoryTask.isNotSolved()) return;

        System.out.println(name + " сдал теорию за " + theoryTask.getAttemptCounter() + " попыток");
        this.wait();
    }

    public ExamTask getPracticalTask(String name) {
        ExamTask examTask = new ExamTask(random.nextInt(2) + 1);
        practicalTasks.put(name, examTask);
        return examTask;
    }

    public int getScore(ExamTask theoryTask, ExamTask practicalTask) {
        int attempts = theoryTask.getAttemptCounter() + practicalTask.getAttemptCounter();
        if (attempts < 4) return 5;
        if (attempts < 8) return 4;
        return 3;
    }
}
