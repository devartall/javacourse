package lesson8.cw.exam;

public class Exam {

    public static void main(String[] args) {
        Teacher teacher = new Teacher();
        Thread student1 = initStudent("Петя", IQ.CLEVER, teacher);
        Thread student2 = initStudent("Вася", IQ.NORMAL, teacher);
        Thread student3 = initStudent("Нина", IQ.NORMAL, teacher);
        Thread student4 = initStudent("Света", IQ.STUPID, teacher);
        Thread student5 = initStudent("Егор", IQ.STUPID, teacher);
        Thread teacherThread = new Thread(teacher);

        Thread[] students = new Thread[]{student1, student2, student3, student4, student5};

        startExam(teacherThread, students);

        waitFinish(students);

        System.out.println("Экзамен завершен!");
    }

    private static void waitFinish(Thread[] students) {
        for (Thread student : students) {
            try {
                student.join();
            } catch (InterruptedException e) {
                System.out.println(student.getName() + " покинул экзамен");
            }
        }
    }

    private static void startExam(Thread teacher, Thread... students) {
        for (Thread student : students) {
            student.start();
        }
        teacher.start();
        System.out.println("Экзамен начат!");
    }

    private static Thread initStudent(String name, IQ iq, Teacher teacher) {
        Thread student = new Thread(new Student(name, teacher, iq));
        student.setName(name);
        return student;
    }
}
