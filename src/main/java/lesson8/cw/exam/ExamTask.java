package lesson8.cw.exam;

public class ExamTask {
    private int rightAnswer;
    private int studentAnswer;
    private int attemptCounter;
    private boolean solved;

    public ExamTask(int rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    public boolean isNotSolved() {
        return !solved;
    }


    public void giveAnswer(int answer) {
        studentAnswer = answer;
    }

    public void increaseAttempt() {
        attemptCounter++;
    }

    public void checkTask() {
        if (studentAnswer == rightAnswer) solved = true;
    }

    public int getAttemptCounter() {
        return attemptCounter;
    }
}
