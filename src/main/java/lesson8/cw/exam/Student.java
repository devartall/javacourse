package lesson8.cw.exam;

import java.util.Random;

public class Student implements Runnable {
    private final String name;
    private final Teacher teacher;
    private final IQ iq;
    private Random random = new Random();

    public Student(String name, Teacher teacher, IQ iq) {
        this.name = name;
        this.teacher = teacher;
        this.iq = iq;
    }

    /**
     * Сдача экзамена
     */
    @Override
    public void run() {
        try {
            System.out.println(name + " пришел на экзамен");
            synchronized (teacher)
            {
                teacher.wait();
            }
            ExamTask theoryTask = teacher.getTheoryTask(name);
            System.out.println(name + " получил теоритическое задание");

            while (theoryTask.isNotSolved()) {
                int answer = prepareAnswer();
                theoryTask.giveAnswer(answer);
                teacher.checkTheoryTask(theoryTask);
            }

            ExamTask practicalTask = teacher.getPracticalTask(name);
            System.out.println(name + " получил практическое задание");

            while (practicalTask.isNotSolved())
            {
                int answer = prepareAnswer();
                practicalTask.giveAnswer(answer);
                teacher.checkPracticalTask(practicalTask);
            }

            int score = teacher.getScore(theoryTask, practicalTask);
            System.out.println(name + " получил за экзамен оценку " + score);
        } catch (InterruptedException e) {
            System.out.println(name + " покинул экзамен");
        }
    }

    private int prepareAnswer() throws InterruptedException {
        int i = (random.nextInt(12) + 8) * 500;
        Thread.sleep(i);
        System.out.println(name + " получил ответ за " + i/1000 + " секунд");
        return random.nextInt(iq.getIq()) + 1;
    }
}
