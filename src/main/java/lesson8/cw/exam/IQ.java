package lesson8.cw.exam;

public enum IQ {
    CLEVER(2),
    NORMAL(3),
    STUPID(4);

    private int iq;

    IQ(int iq) {
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }
}
