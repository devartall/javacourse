package lesson8.cw;

public class CarTester {
    public static void main(String[] args) throws InterruptedException {
        Object lock = new Object();
        CarTask carTask1 = new CarTask(new Car(lock));
        CarTask carTask2 = new CarTask(new Car(lock));
        CarTask carTask3 = new CarTask(new Car(lock));
        CarTask carTask4 = new CarTask(new Car(lock));
        CarTask carTask5 = new CarTask(new Car(lock));

        Thread thread1 = new Thread(carTask1);
        Thread thread2 = new Thread(carTask2);
        Thread thread3 = new Thread(carTask3);
        Thread thread4 = new Thread(carTask4);
        Thread thread5 = new Thread(carTask5);

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

        thread1.join();
        thread2.join();
        thread3.join();
        thread4.join();
        thread5.join();
    }
}
