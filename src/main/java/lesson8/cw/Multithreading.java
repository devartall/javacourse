package lesson8.cw;

public class Multithreading {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Start main thread");
        Task task = new Task();
//        Task task2 = null;
        Thread thread1 = new Thread(task);
        Thread thread2 = new Thread(task);

//        task1 = new Task(thread2, 5000);
//        task2 = new Task(thread1, 5000);

        thread1.setName("thread_1");
        thread2.setName("thread_2");

        thread1.setDaemon(true);
        thread2.setDaemon(true);

        thread1.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                System.out.println("cathed exp with handler" + throwable.getClass());
            }
        });

        try {
            thread1.start();
            thread2.start();
        } catch (RuntimeException e) {
            System.out.println("catch exp " + e.getClass());
        }

        thread1.join();
        thread2.join();


        System.out.println("End main thread");
    }
}
