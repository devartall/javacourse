package lesson8.cw;

public class Car {
    private Object lock;

    public Car(Object lock) {
        this.lock = lock;

    }

    public synchronized void drive() {
        System.out.println("drive");
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName());
    }

    public static synchronized void driveStatic() {
        System.out.println("driveStatic");
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName());
    }

    public void driveObj() {
        System.out.println("driveObj");
        synchronized (lock) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName());
        }
    }
}
