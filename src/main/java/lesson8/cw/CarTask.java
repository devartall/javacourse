package lesson8.cw;

public class CarTask implements Runnable {
    private Car car;

    public CarTask(Car car) {
        this.car = car;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
        car.driveStatic();
    }
}
