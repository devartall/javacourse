package lesson8.cw;

public class Task implements Runnable {
    private Thread thread;
    private int waitTime;

    public Task(Thread thread, int waitTime) {
        this.thread = thread;
        this.waitTime = waitTime;
    }

    public Task() {
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " start");
        if (true) throw new RuntimeException();
        System.out.println(Thread.currentThread().getName() + " end");
    }
}
