package lesson5.cw.bank;

public class Worker extends AbstractHuman {
    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Worker(String name, String position) {
        this.position = position;
        this.setName(name);
    }
}
