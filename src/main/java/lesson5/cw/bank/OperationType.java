package lesson5.cw.bank;

public enum OperationType {
    OPEN_DEBIT,
    UPDATE_DEBIT,
    OPEN_CREDIT
}
