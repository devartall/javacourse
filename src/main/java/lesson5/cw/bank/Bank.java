package lesson5.cw.bank;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class Bank {
    List<Worker> workers;
    List<IAccount> accounts;

    public Bank() {
        this.workers = new ArrayList<>();
        workers.add(new Worker("Ivanov II", "first class officer"));
        workers.add(new Worker("Petrov PP", "second class officer"));
        workers.add(new Worker("Sidorov SS", "junior officer"));

        accounts = new ArrayList<>();
    }

    public boolean performOperation(OperationType type, long userId) {
        if (type.equals(OperationType.OPEN_DEBIT)) {
            Random workerRandom = new Random();
            int i = workerRandom.nextInt(3);
            System.out.println("Client was served by employee#" + workers.get(i).getName());
            Debit debit = new Debit();
            debit.setAmount(100L);
            debit.setClientId(userId);
            accounts.add(debit);
            System.out.println("Account was created at index " + accounts.size());

            return true;
        }
        return false;
    }

    public static void main(String [] args) {
        Bank bank = new Bank();
        List<Client> clients = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            Client client = new Client("name".concat(String.valueOf(i)));
            clients.add(client);
        }

        int iterationIndex = 0;

        while(iterationIndex < 10) {
            Random random = new Random();
            int numberOfActiveClients = random.nextInt(20);

            for (int currentClientIndex = 0; currentClientIndex < numberOfActiveClients; currentClientIndex++) {
                Random clientRandom = new Random();
                Client currentClient = clients.get(clientRandom.nextInt(20));
                Random operationRandom = new Random();
                int operationIndex = operationRandom.nextInt(3);
                OperationType operationType = null;
                if (operationIndex == 0) {
                    operationType = OperationType.OPEN_DEBIT;
                } else if (operationIndex == 1) {
                    operationType = OperationType.UPDATE_DEBIT;
                } else if (operationIndex == 2) {
                    operationType = OperationType.OPEN_CREDIT;
                }

                System.out.println("Day #" + iterationIndex + ". Active client: " + currentClient.getName() + ", operation: " + operationType.toString());
                bank.performOperation(operationType, currentClient.getId());
            }
            System.out.println(" ");
            iterationIndex++;
        }
    }
}
