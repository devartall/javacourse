package lesson5.cw.bank;

public interface IAccount {
    long getClientId();
    void setClientId(long clientId);
    long getAmount();
    void setAmount(long setAmount);
}
