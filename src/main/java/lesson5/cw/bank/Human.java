package lesson5.cw.bank;

public interface Human {
    String getName();
    void setName(String name);
}
