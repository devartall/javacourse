package lesson5.cw.bank;

public class Client extends AbstractHuman {
    private static long CLIENT_COUNT = 0L;

    public Client(String name) {
        this.setName(name);
        this.setId(CLIENT_COUNT);
        CLIENT_COUNT++;
    }
}
