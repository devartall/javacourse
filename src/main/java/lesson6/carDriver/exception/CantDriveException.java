package lesson6.carDriver.exception;

public class CantDriveException extends Exception {
    public CantDriveException(Exception e) {
        initCause(e);
    }

    public CantDriveException() {

    }
}
