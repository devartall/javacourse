package lesson6;

import lesson6.car.exception.TireException;
import lesson6.carDriver.exception.CantDriveException;
import lesson6.exception.UfoStolenException;

public class Travel {
    public static void main(String[] args) {
        Car car = new Car();
        CarDriver carDriver = new CarDriver(car);

        try {
            carDriver.ride();
        } catch (CantDriveException e) {
            if (e.getCause() instanceof UfoStolenException) {
                killAliens();
            }
            System.out.println("Cause of cant ride - " + e.getCause());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("Ride ended");
        }

        System.out.println("Happy end!");
    }

    private static void killAliens() {
        System.out.println("Save life");
    }
}
