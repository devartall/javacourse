package lesson6;

import lesson6.car.exception.TireException;
import lesson6.carDriver.exception.CantDriveException;

public class CarDriver {
    private final Car car;

    public CarDriver(Car car) {
        this.car = car;
    }

    public void ride() throws Exception {
        System.out.println("Driver start ride");

        try (Car car1 = car){
            //car.openCar();
            car.drive();
        } catch (Exception e) {
            throw new CantDriveException(e);
        } finally {
//            car.close();
        }

        if (car.isLocked()) {
            System.out.println("CarDriver: Car closed");
        } else {
            System.out.println("CarDriver: Car open");
        }
    }

    private void repairWheel() {
        System.out.println("Wheel repaired");
    }
}
