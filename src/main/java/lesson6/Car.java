package lesson6;

import lesson6.car.exception.KeyBrokenException;
import lesson6.exception.UfoStolenException;

public class Car implements AutoCloseable{
    private boolean isLocked = true;

    public void drive() {
        System.out.println("Car start drive");
//        throw new TireException();
        throw new UfoStolenException();
    }

    public void openCar() {
        isLocked = false;
        System.out.println("Car opened");
    }

    public boolean isLocked() {
        return isLocked;
    }

    @Override
    public void close() throws Exception {
        if (isLocked) {
            throw new KeyBrokenException();
        }
        System.out.println("Car closed");
    }
}
