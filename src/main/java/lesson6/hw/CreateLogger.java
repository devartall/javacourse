package lesson6.hw;

import java.io.IOException;
import java.util.logging.*;

/**
 * Написать класс наследник Car - SuperCar, которая может самостоятельно справиться с TireException.
 * Однако метод drive может выбросить CarOverheatingException.
 * Водитель должен уметь справляться с CarOverheatingException.
 *
 * Также переписать все SystemOut через logger's.
 * В случае обычных действий использовать уровень INFO,
 * в случае возникновения исключений логгировать их уровнем WARNING
 *
 * Ниже примеры настройки и создания логгеров
 */
public class CreateLogger {
    private static final Logger LOGGER = Logger.getLogger(CreateLogger.class.getName());

    public static void main(String[] args) {
        LOGGER.info("Logger Name: " + LOGGER.getName());

        LOGGER.warning("Can cause ArrayIndexOutOfBoundsException");

        //Creating consoleHandler and fileHandler
        Handler consoleHandler = null;
        Handler fileHandler  = null;

        consoleHandler = new ConsoleHandler();
        try {
            fileHandler  = new FileHandler("./output.log");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Assigning handlers to LOGGER object
        LOGGER.addHandler(consoleHandler);
        LOGGER.addHandler(fileHandler);

        //Setting levels to handlers and LOGGER
        consoleHandler.setLevel(Level.ALL);
        fileHandler.setLevel(Level.ALL);
        LOGGER.setLevel(Level.ALL);

        // Creating SimpleFormatter
        Formatter simpleFormatter = new SimpleFormatter();

        // Assigning handler to logger
        LOGGER.addHandler(fileHandler);

        // Logging message of Level info (this should be publish in the default format i.e. XMLFormat)
        LOGGER.info("Finnest message: Logger with DEFAULT FORMATTER");

        // Setting formatter to the handler
        fileHandler.setFormatter(simpleFormatter);

        LOGGER.config("Configuration done.");
    }
}
