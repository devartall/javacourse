package lesson17.foodmachine.oop;

import lesson17.foodmachine.oop.bank.Bank;
import lesson17.foodmachine.oop.bank.Card;
import lesson17.foodmachine.oop.discount.WeekendDiscountStrategy;
import lesson17.foodmachine.oop.machine.FoodMachine;
import lesson17.foodmachine.oop.machine.FoodMachineBuilder;
import lesson17.foodmachine.oop.phone.Phone;
import lesson17.foodmachine.oop.phone.PhoneServer;
import org.junit.Test;

import static lesson17.foodmachine.oop.machine.StateMessage.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-12
 */
public class FoodMachineTest {

    @Test
    public void test_CHOOSE_PRODUCT() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        assertEquals(CHOOSE_PRODUCT.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_CHOOSE_ANOTHER_PRODUCT() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        machine.chooseProduct(14);

        assertEquals(CHOOSE_ANOTHER_PRODUCT.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_PRODUCT_IS_NOT_SELECTED_when_activateCard() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        machine.activateCard();

        assertEquals(PRODUCT_IS_NOT_SELECTED.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_CARD_IS_NOT_ACTIVATED_when_touchCard() {
        Card card = new Card("0000-0000-0000-0000");
        Bank bank = new Bank();
        bank.addCard(card, 1000);

        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(bank)
            .setServer(new PhoneServer())
            .buildFitness();

        machine.touchCard(card);

        assertEquals(CARD_IS_NOT_ACTIVATED.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_PRODUCT_IS_NOT_SELECTED_when_dropCoin() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        machine.dropCoin(10);

        assertEquals(PRODUCT_IS_NOT_SELECTED.getText(), machine.getMessage());
        assertEquals(10, machine.getSum());
    }

    @Test
    public void test_cancel() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        machine.dropCoin(10);
        machine.cancel();

        assertEquals("Сдача: 10\nВыберите продукт", machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_PAY_PRODUCT() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        machine.chooseProduct(13);

        assertEquals(PAY_PRODUCT.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_CLICK_RESET() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        machine.chooseProduct(13);
        machine.chooseProduct(21);

        assertEquals(CLICK_RESET.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_dropCoin() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        machine.chooseProduct(13);
        machine.dropCoin(10);

        assertEquals(SUM_IS.getText() + " 10", machine.getMessage());
        assertEquals(10, machine.getSum());

        machine.dropCoin(20);

        assertEquals(SUM_IS.getText() + " 30", machine.getMessage());
        assertEquals(30, machine.getSum());
    }

    @Test
    public void test_dropCoin_more_enough() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        machine.chooseProduct(13);
        machine.dropCoin(100);

        assertEquals("Салат из свежих овощей\n" + REMAINS.getText() + " 30", machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_PAY_WITH_CARD() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        machine.chooseProduct(31);
        machine.activateCard();

        assertEquals(PAY_WITH_CARD.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_touchCard() {
        Card card = new Card("0000-0000-0000-0000");
        Bank bank = new Bank();
        bank.addCard(card, 1000);

        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(bank)
            .setServer(new PhoneServer())
            .buildFitness();

        machine.chooseProduct(31);
        machine.activateCard();
        machine.touchCard(card);

        assertEquals("Сырник\n" + REMAINS.getText() + " 0", machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_NON_SUFFICIENT_FUNDS() {
        Card card = new Card("0000-0000-0000-0000");
        Bank bank = new Bank();
        bank.addCard(card, 10);

        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(bank)
            .setServer(new PhoneServer())
            .buildFitness();

        machine.chooseProduct(31);
        machine.activateCard();
        machine.touchCard(card);

        assertEquals("Недостаточно средств", machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_all_chooseProduct() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        machine.chooseProduct(11);
        machine.dropCoin(50);
        assertEquals("Вода без газа\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(12);
        machine.dropCoin(120);
        assertEquals("Протеиновый батончик\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(13);
        machine.dropCoin(70);
        assertEquals("Салат из свежих овощей\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(21);
        machine.dropCoin(70);
        assertEquals("Рисовая каша\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(22);
        machine.dropCoin(40);
        assertEquals("Банан\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(23);
        machine.dropCoin(150);
        assertEquals("Протеиновый коктейль\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(31);
        machine.dropCoin(170);
        assertEquals("Сырник\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(32);
        machine.dropCoin(150);
        assertEquals("Паста с овощами\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(33);
        machine.dropCoin(190);
        assertEquals("Куриная грудка\n" + REMAINS.getText() + " 0", machine.getMessage());
    }

    @Test
    public void test_emptyCell() {
        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(new PhoneServer())
            .buildFitness();

        for (int i = 0; i < 5; i++) {
            machine.chooseProduct(11);
            machine.dropCoin(50);
        }

        machine.chooseProduct(11);
        assertEquals(CHOOSE_ANOTHER_PRODUCT.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }


    /*---------------------------OBSERVER---------------------------*/


    @Test
    public void test_makeDiscount() {
        PhoneServer server = new PhoneServer();
        Phone phone_1 = new Phone("+7 (999) 123-45-67");
        Phone phone_2 = new Phone("+7 (999) 987-65-43");
        Phone phone_3 = new Phone("+7 (999) 111-22-33");
        server.addPhone(phone_1.getNumber(), phone_1);
        server.addPhone(phone_2.getNumber(), phone_2);
        server.addPhone(phone_3.getNumber(), phone_3);

        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(server)
            .buildFitness();

        machine.subscribe(phone_1.getNumber());
        machine.subscribe(phone_3.getNumber());

        assertNull(phone_1.getScreenText());
        assertNull(phone_2.getScreenText());
        assertNull(phone_3.getScreenText());

        machine.makeDiscount("Протеиновый коктейль");

        assertEquals("Протеиновый коктейль теперь стоит 135", phone_1.getScreenText());
        assertNull(phone_2.getScreenText());
        assertEquals("Протеиновый коктейль теперь стоит 135", phone_3.getScreenText());
    }

    @Test
    public void test_makeDiscount_setStrategy() {
        PhoneServer server = new PhoneServer();
        Phone phone_1 = new Phone("+7 (999) 123-45-67");
        Phone phone_2 = new Phone("+7 (999) 987-65-43");
        Phone phone_3 = new Phone("+7 (999) 111-22-33");
        server.addPhone(phone_1.getNumber(), phone_1);
        server.addPhone(phone_2.getNumber(), phone_2);
        server.addPhone(phone_3.getNumber(), phone_3);

        FoodMachine machine = FoodMachineBuilder.createBuilder()
            .setBank(new Bank())
            .setServer(server)
            .buildFitness();

        machine.subscribe(phone_1.getNumber());
        machine.subscribe(phone_3.getNumber());

        assertNull(phone_1.getScreenText());
        assertNull(phone_2.getScreenText());
        assertNull(phone_3.getScreenText());

        machine.setDiscountStrategy(new WeekendDiscountStrategy());
        machine.makeDiscount("Протеиновый коктейль");

        assertEquals("Протеиновый коктейль теперь стоит 105", phone_1.getScreenText());
        assertNull(phone_2.getScreenText());
        assertEquals("Протеиновый коктейль теперь стоит 105", phone_3.getScreenText());
    }
}
