package lesson17.foodmachine.procedural;

import lesson17.foodmachine.procedural.phone.Phone;
import lesson17.foodmachine.procedural.phone.PhoneServer;
import org.junit.Test;

import static lesson17.foodmachine.oop.machine.StateMessage.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-13
 */
public class HardFoodMachineTest {

    @Test
    public void test_CHOOSE_PRODUCT() {
        HardFoodMachine machine = new HardFoodMachine();

        assertEquals(CHOOSE_PRODUCT.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_CHOOSE_ANOTHER_PRODUCT() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.chooseProduct(14);

        assertEquals(CHOOSE_ANOTHER_PRODUCT.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_PRODUCT_IS_NOT_SELECTED_when_activateCard() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.activateCard();

        assertEquals(PRODUCT_IS_NOT_SELECTED.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_CARD_IS_NOT_ACTIVATED_when_touchCard() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.touchCard();

        assertEquals(CARD_IS_NOT_ACTIVATED.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_PRODUCT_IS_NOT_SELECTED_when_dropCoin() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.dropCoin(10);

        assertEquals(PRODUCT_IS_NOT_SELECTED.getText(), machine.getMessage());
        assertEquals(10, machine.getSum());
    }

    @Test
    public void test_cancel() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.dropCoin(10);
        machine.cancel();

        assertEquals("Сдача: 10\nВыберите продукт", machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_PAY_PRODUCT() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.chooseProduct(13);

        assertEquals(PAY_PRODUCT.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_CLICK_RESET() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.chooseProduct(13);
        machine.chooseProduct(21);

        assertEquals(CLICK_RESET.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_dropCoin() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.chooseProduct(13);
        machine.dropCoin(10);

        assertEquals(SUM_IS.getText() + " 10", machine.getMessage());
        assertEquals(10, machine.getSum());

        machine.dropCoin(20);

        assertEquals(SUM_IS.getText() + " 30", machine.getMessage());
        assertEquals(30, machine.getSum());
    }

    @Test
    public void test_dropCoin_more_enough() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.chooseProduct(13);
        machine.dropCoin(100);

        assertEquals("Кисель вишневый\n" + REMAINS.getText() + " 30", machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_PAY_WITH_CARD() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.chooseProduct(31);
        machine.activateCard();

        assertEquals(PAY_WITH_CARD.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_touchCard() {
        HardFoodMachine machine = new HardFoodMachine();
        machine.chooseProduct(31);
        machine.activateCard();
        machine.touchCard();

        assertEquals("Картофель с котлетой\n" + REMAINS.getText() + " 0", machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_all_chooseProduct() {
        HardFoodMachine machine = new HardFoodMachine();

        machine.chooseProduct(11);
        machine.dropCoin(50);
        assertEquals("Вода без газа\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(12);
        machine.dropCoin(60);
        assertEquals("Печенье с изюмом\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(13);
        machine.dropCoin(70);
        assertEquals("Кисель вишневый\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(21);
        machine.dropCoin(80);
        assertEquals("Молочный шоколад\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(22);
        machine.dropCoin(90);
        assertEquals("Ассорти орехов\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(23);
        machine.dropCoin(100);
        assertEquals("Бутерброд с ветчиной\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(31);
        machine.dropCoin(110);
        assertEquals("Картофель с котлетой\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(32);
        machine.dropCoin(120);
        assertEquals("Бургер с курицей\n" + REMAINS.getText() + " 0", machine.getMessage());

        machine.chooseProduct(33);
        machine.dropCoin(130);
        assertEquals("Салат с креветками\n" + REMAINS.getText() + " 0", machine.getMessage());
    }

    @Test
    public void test_emptyCell() {
        HardFoodMachine machine = new HardFoodMachine();

        for (int i = 0; i < 5; i++) {
            machine.chooseProduct(11);
            machine.dropCoin(50);
        }

        machine.chooseProduct(11);
        assertEquals(CHOOSE_ANOTHER_PRODUCT.getText(), machine.getMessage());
        assertEquals(0, machine.getSum());
    }

    @Test
    public void test_makeDiscount() {
        PhoneServer server = new PhoneServer();

        HardFoodMachine machine = new HardFoodMachine();
        machine.setPhoneServer(server);

        Phone phone_1 = new Phone("+7 (999) 123-45-67");
        Phone phone_2 = new Phone("+7 (999) 987-65-43");
        Phone phone_3 = new Phone("+7 (999) 111-22-33");
        server.addPhone(phone_1);
        server.addPhone(phone_2);
        server.addPhone(phone_3);

        assertNull(phone_1.getScreenText());
        assertNull(phone_2.getScreenText());
        assertNull(phone_3.getScreenText());

        machine.makeDiscount("Ассорти орехов", HardFoodMachine.GENERAL, false);

        assertEquals("Ассорти орехов теперь стоит 81", phone_1.getScreenText());
        assertEquals("Ассорти орехов теперь стоит 81", phone_2.getScreenText());
        assertEquals("Ассорти орехов теперь стоит 81", phone_3.getScreenText());
    }
}
