package lesson4.task;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WonderfulStringTest {

    @Test
    public void test_solution_1() {
        boolean result = WonderfulString.solution("");

        assertTrue(result);
    }

    @Test
    public void test_solution_2() {
        boolean result = WonderfulString.solution("a");

        assertTrue(result);
    }

    @Test
    public void test_solution_3() {
        boolean result = WonderfulString.solution("aba");

        assertFalse(result);
    }

    @Test
    public void test_solution_4() {
        boolean result = WonderfulString.solution("aaabbbcdcdcd");

        assertTrue(result);
    }

    @Test
    public void test_solution_5() {
        boolean result = WonderfulString.solution("ffhdrhdrzbb");

        assertFalse(result);
    }

    @Test
    public void test_solution_6() {
        boolean result = WonderfulString.solution("aaaabc");

        assertFalse(result);
    }
}
