package lesson4.task;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SelectionSortTest {

    @Test
    public void test_solution_1() {
        int[] arr = {};

        SelectionSort.solution(arr);

        assertArrayEquals(new int[] {}, arr);
    }

    @Test
    public void test_solution_2() {
        int[] arr = {1};

        SelectionSort.solution(arr);

        assertArrayEquals(new int[] {1}, arr);
    }

    @Test
    public void test_solution_3() {
        int[] arr = {2, 1};

        SelectionSort.solution(arr);

        assertArrayEquals(new int[] {1, 2}, arr);
    }

    @Test
    public void test_solution_4() {
        int[] arr = {5, 7, 1, 0, -4, 3, 9, 8, 6, 9};

        SelectionSort.solution(arr);

        assertArrayEquals(new int[] {-4, 0, 1, 3, 5, 6, 7, 8, 9, 9}, arr);
    }

    @Test
    public void test_solution_5() {
        int[] arr = {3, 3, 3, 3, 3};

        SelectionSort.solution(arr);

        assertArrayEquals(new int[] {3, 3, 3, 3, 3}, arr);
    }

    @Test
    public void test_solution_6() {
        int[] arr = {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};

        SelectionSort.solution(arr);

        assertArrayEquals(new int[] {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5}, arr);
    }

    @Test
    public void test_solution_7() {
        int[] arr = {100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90};

        SelectionSort.solution(arr);

        assertArrayEquals(new int[] {90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100}, arr);
    }

    @Test
    public void test_solution_8() {
        int[] arr = {1000, 456, 23, 0, -9999, 521, 789, 159, -0, -531};

        SelectionSort.solution(arr);

        assertArrayEquals(new int[] {-9999, -531, 0, 0, 23, 159, 456, 521, 789, 1000}, arr);
    }

    @Test
    public void test_solution_9() {
        int[] arr = {-3, 5, 9, 3, -1, -6, -4, 5, 7, 8, 3, 7, 0, -2, -1, -1, -1};

        SelectionSort.solution(arr);

        assertArrayEquals(new int[] {-6, -4, -3, -2, -1, -1, -1, -1, 0, 3, 3, 5, 5, 7, 7, 8, 9}, arr);
    }

    @Test
    public void test_solution_10() {
        int[] arr = {3, 2, 1, 3, 2, 1, 1, 2, 3, 1, 2, 3, 2, 1, 3, 2, 1, 3};

        SelectionSort.solution(arr);

        assertArrayEquals(new int[] {1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3}, arr);
    }
}
