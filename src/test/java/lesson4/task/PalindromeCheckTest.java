package lesson4.task;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PalindromeCheckTest {

    @Test
    public void test_solution_1() {
        boolean result = PalindromeCheck.solution("");

        assertTrue(result);
    }

    @Test
    public void test_solution_2() {
        boolean result = PalindromeCheck.solution("Q");

        assertTrue(result);
    }

    @Test
    public void test_solution_3() {
        boolean result = PalindromeCheck.solution("заказ");

        assertTrue(result);
    }

    @Test
    public void test_solution_4() {
        boolean result = PalindromeCheck.solution("заказы");

        assertFalse(result);
    }

    @Test
    public void test_solution_5() {
        boolean result = PalindromeCheck.solution("И к вам и трем с смерти мавки");

        assertTrue(result);
    }

    @Test
    public void test_solution_6() {
        boolean result = PalindromeCheck.solution("    И  к вам и      трем с смерти  мавки");

        assertTrue(result);
    }

    @Test
    public void test_solution_7() {
        boolean result = PalindromeCheck.solution("abcdecba");

        assertFalse(result);
    }

    @Test
    public void test_solution_8() {
        boolean result = PalindromeCheck.solution("AB");

        assertFalse(result);
    }

    @Test
    public void test_solution_9() {
        boolean result = PalindromeCheck.solution("AaAa");

        assertTrue(result);
    }

    @Test
    public void test_solution_10() {
        boolean result = PalindromeCheck.solution("aa");

        assertTrue(result);
    }

    @Test
    public void test_solution_11() {
        boolean result = PalindromeCheck.solution("aaa");

        assertTrue(result);
    }

    @Test
    public void test_solution_12() {
        boolean result = PalindromeCheck.solution("aba");

        assertTrue(result);
    }
}
