package lesson4.task;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class LongestCommonSubstringTest {

    @Test
    public void test_solution_1() {
        String result = LongestCommonSubstring.solution("", "");

        assertEquals("-1", result);
    }

    @Test
    public void test_solution_2() {
        String result = LongestCommonSubstring.solution("a", "a");

        assertEquals("a", result);
    }

    @Test
    public void test_solution_3() {
        String result = LongestCommonSubstring.solution("a", "b");

        assertEquals("-1", result);
    }

    @Test
    public void test_solution_4() {
        String result = LongestCommonSubstring.solution("aba", "cfb");

        assertEquals("b", result);
    }

    @Test
    public void test_solution_5() {
        String result = LongestCommonSubstring.solution("rjqobjsberlstbk", "jqgqkvpssberkgo");

        assertEquals("sber", result);
    }

    @Test
    public void test_solution_6() {
        String result = LongestCommonSubstring.solution("abcd", "efg");

        assertEquals("-1", result);
    }

    @Test
    public void test_solution_7() {
        String result = LongestCommonSubstring.solution("sdddfdddd", "kkddddrtddd");

        assertEquals("dddd", result);
    }

    @Test
    public void test_solution_8() {
        String result = LongestCommonSubstring.solution("zzxdfg", "uiopzzzzz");

        assertEquals("zz", result);
    }

    @Test
    public void test_solution_9() {
        String result = LongestCommonSubstring.solution("uiuiuiuiu", "iuiuiuiuiuiuiuiu");

        assertEquals("uiuiuiuiu", result);
    }

    @Test
    public void test_solution_10() {
        String result = LongestCommonSubstring.solution("sberbank", "sberbank");

        assertEquals("sberbank", result);
    }

    @Test
    public void test_solution_11() {
        String result = LongestCommonSubstring.solution("sberagilebank", "sbersfbank");

        assertEquals("sber", result);
    }

    @Test
    public void test_solution_12() {
        String result = LongestCommonSubstring.solution("abc", "ade");

        assertEquals("a", result);
    }
}
