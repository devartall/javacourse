package lesson4.task;

import org.junit.Test;

import static org.junit.Assert.*;

public class BracketsCheckTest {

    @Test
    public void test_solution_1() {

        boolean result = BracketsCheck.solution("{}([{}]([]))");

        assertTrue(result);
    }

    @Test
    public void test_solution_2() {

        boolean result = BracketsCheck.solution("[]([{]})");

        assertFalse(result);
    }

    @Test
    public void test_solution_3() {

        boolean result = BracketsCheck.solution("");

        assertTrue(result);
    }

    @Test
    public void test_solution_4() {

        boolean result = BracketsCheck.solution("[");

        assertFalse(result);
    }

    @Test
    public void test_solution_5() {

        boolean result = BracketsCheck.solution(")");

        assertFalse(result);
    }

    @Test
    public void test_solution_6() {

        boolean result = BracketsCheck.solution("()[]{}");

        assertTrue(result);
    }

    @Test
    public void test_solution_7() {

        boolean result = BracketsCheck.solution("[[[()[{}]]]]");

        assertTrue(result);
    }

    @Test
    public void test_solution_8() {

        boolean result = BracketsCheck.solution("([[[[()[{}]]]])()");

        assertFalse(result);
    }

    @Test
    public void test_solution_9() {

        boolean result = BracketsCheck.solution("({[([{}}");

        assertFalse(result);
    }

    @Test
    public void test_solution_10() {

        boolean result = BracketsCheck.solution("({}{}[]))))))");

        assertFalse(result);
    }

    @Test
    public void test_solution_11() {

        boolean result = BracketsCheck.solution("{()}[[([[][][]{()}])]]");

        assertTrue(result);
    }

    @Test
    public void test_solution_12() {

        boolean result = BracketsCheck.solution("()[]{}[]{}()");

        assertTrue(result);
    }

    @Test
    public void test_solution_13() {

        boolean result = BracketsCheck.solution("(((({{{{[[[[]]]]}}}}))))");

        assertTrue(result);
    }

    @Test
    public void test_solution_14() {

        boolean result = BracketsCheck.solution("([}");

        assertFalse(result);
    }

    @Test
    public void test_solution_15() {

        boolean result = BracketsCheck.solution("}(){}");

        assertFalse(result);
    }

    @Test
    public void test_solution_16() {

        boolean result = BracketsCheck.solution("()((){}[]");

        assertFalse(result);
    }
}
