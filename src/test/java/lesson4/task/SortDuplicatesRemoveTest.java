package lesson4.task;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SortDuplicatesRemoveTest {

    @Test
    public void test_solution_1() {
        int[] arr = new int[] {0, 0, 1, 2, 6, 7, 7, 7, 9, 10, 10, 10, 10};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {0, 1, 2, 6, 7, 9, 10}, result);
    }

    @Test
    public void test_solution_2() {
        int[] arr = new int[] {13, 13, 13, 13, 13, 13, 13, 13, 13, 13};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {13}, result);
    }

    @Test
    public void test_solution_3() {
        int[] arr = new int[] {10, 10, 20, 20, 30, 30, 40, 50, 60, 70, 70, 70, 80, 80, 99};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {10, 20, 30, 40, 50, 60, 70, 80, 99}, result);
    }

    @Test
    public void test_solution_4() {
        int[] arr = new int[] {};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {}, result);
    }

    @Test
    public void test_solution_5() {
        int[] arr = new int[] {0};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {0}, result);
    }

    @Test
    public void test_solution_6() {
        int[] arr = new int[] {1, 1, 1, 1, 99, 99, 99, 99};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {1, 99}, result);
    }

    @Test
    public void test_solution_7() {
        int[] arr = new int[] {56, 65, 67, 87, 87, 87};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {56, 65, 67, 87}, result);
    }

    @Test
    public void test_solution_8() {
        int[] arr = new int[] {23, 23, 34, 34, 45, 45, 56, 56};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {23, 34, 45, 56}, result);
    }

    @Test
    public void test_solution_9() {
        int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, result);
    }

    @Test
    public void test_solution_10() {
        int[] arr = new int[] {1, 2, 3, 4, 5, 5, 5, 5, 5, 6, 7, 8, 9};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9}, result);
    }

    @Test
    public void test_solution_11() {
        int[] arr = new int[] {0, 1, 2, 3};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {0, 1, 2, 3}, result);
    }

    @Test
    public void test_solution_12() {
        int[] arr = new int[] {0, 0, 1, 2, 3};

        int[] result = SortDuplicatesRemove.solution(arr);

        assertArrayEquals(new int[] {0, 1, 2, 3}, result);
    }
}
