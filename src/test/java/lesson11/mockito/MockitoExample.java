package lesson11.mockito;

import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-08
 */
public class MockitoExample {

    @Test
    public void mock_example() {
        CashMachine cashMachine = Mockito.mock(CashMachine.class);

        System.out.println(cashMachine.takeMoney(100));
        System.out.println(cashMachine.giveMoney(100));
    }

    @Test
    public void mock_with_when_example() {
        CashMachine cashMachine = Mockito.mock(CashMachine.class);
        when(cashMachine.takeMoney(100)).thenReturn(true);
        when(cashMachine.giveMoney(100)).thenReturn(1000);

        System.out.println(cashMachine.takeMoney(100));
        System.out.println(cashMachine.giveMoney(100));
    }

    @Test
    public void spy_example() {
        CashMachine cashMachine = Mockito.spy(CashMachine.class);
        //when(cashMachine.takeMoney(100)).thenReturn(true);
        //when(cashMachine.giveMoney(100)).thenReturn(1000);

        System.out.println(cashMachine.takeMoney(100));
        System.out.println(cashMachine.giveMoney(100));
    }

    @Test
    public void spy_with_when_example() {
        CashMachine cashMachine = Mockito.spy(CashMachine.class);
        when(cashMachine.takeMoney(100)).thenReturn(false);
        when(cashMachine.giveMoney(100)).thenReturn(0);

        System.out.println(cashMachine.takeMoney(100));
        System.out.println(cashMachine.giveMoney(100));
    }

    @Test
    public void doAnswer_example() {
        CashMachine cashMachine = Mockito.spy(CashMachine.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object argument = invocation.getArguments()[0];
                System.out.println("Money: " + argument);
                return invocation.callRealMethod();
            }
        }).when(cashMachine).takeMoney(100);

        System.out.println(cashMachine.takeMoney(100));
    }

    @Test
    public void doAnswer_with_void_example() {
        CashMachine cashMachine = Mockito.spy(CashMachine.class);

        cashMachine.takeMoney(100);
        cashMachine.giveMoney(100);

        cashMachine.printLog();

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                cashMachine.getLog().add("printLog");
                invocation.callRealMethod();
                return null;
            }
        }).when(cashMachine).printLog();

        System.out.println("-------");
        cashMachine.printLog();
    }

    @Test
    public void doNothing_example() {
        CashMachine cashMachine = Mockito.spy(CashMachine.class);

        doNothing().when(cashMachine).addLog(anyString());

        cashMachine.takeMoney(100);
        cashMachine.giveMoney(100);

        cashMachine.printLog();
    }

    @Test(expected = IllegalArgumentException.class)
    public void doThrow_example() {
        CashMachine cashMachine = Mockito.spy(CashMachine.class);

        doThrow(new IllegalArgumentException("negative sum"))
            .when(cashMachine).giveMoney(-1);

        cashMachine.giveMoney(-1);
    }

    @Test
    public void inOrder_example() {
        CashMachine cashMachine_1 = Mockito.mock(CashMachine.class);
        CashMachine cashMachine_2 = Mockito.mock(CashMachine.class);

        cashMachine_1.giveMoney(100);
        cashMachine_2.giveMoney(100);

        InOrder inOrder = inOrder(cashMachine_1, cashMachine_2);

        inOrder.verify(cashMachine_1).giveMoney(100);
        inOrder.verify(cashMachine_2).giveMoney(100);
    }
}
