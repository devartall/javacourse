package lesson11.mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-09
 */
public class CashMachine {

    private List<String> log = new ArrayList<>();

    public boolean takeMoney(int sum) {
        addLog("takeMoney");
        return true;
    }

    public int giveMoney(int sum) {
        if (sum < 0) {
            addLog("negative sum");
            return 0;
        } else {
            addLog("giveMoney");
            return sum;
        }
    }

    public List<String> getLog() {
        return log;
    }

    public void addLog(String s) {
        log.add(s);
    }

    public void printLog() {
        StringJoiner joiner = new StringJoiner("\n");
        log.forEach(joiner::add);

        System.out.println(joiner);
    }
}
