package lesson11.shop;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-08
 */
public class CartTest {

    @Test
    public void test_clear() {
        Cart cart = new Cart();
        cart.addGood(Good.BREAD);
        cart.addGood(Good.MEAT);
        cart.addGood(Good.MILK);
        cart.addGood(Good.MILK);

        List<Good> goods = cart.clear();

        assertThat(goods, hasSize(4));
        assertSame(Good.BREAD, goods.get(0));
        assertSame(Good.MEAT, goods.get(1));
        assertSame(Good.MILK, goods.get(2));
        assertSame(Good.MILK, goods.get(3));
    }
}
