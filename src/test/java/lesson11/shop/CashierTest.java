package lesson11.shop;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-09
 */
public class CashierTest {

    @Test
    public void test_calculateSum() {
        List<Good> goods = new ArrayList<>();
        goods.add(Good.BREAD);
        goods.add(Good.BREAD);
        goods.add(Good.BREAD);
        goods.add(Good.MILK);
        goods.add(Good.MILK);
        goods.add(Good.MEAT);

        assertEquals(550, new Cashier().calculateSum(goods));
    }
}
