package lesson11.shop;

import org.junit.Before;
import org.junit.Test;

import static lesson11.shop.Shop.MAX_CARTS;
import static org.junit.Assert.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-08
 */
public class ShopTest {

    private Shop shop;

    @Before
    public void setUp() {
        this.shop = new Shop();
    }

    @Test(expected = RuntimeException.class)
    public void test_addCart_whenExceededLimit() {
        for (int i = 0; i < MAX_CARTS + 1; i++)
            shop.addCart(new Cart());
    }

    @Test
    public void test_removeCart() {
        assertNotNull(shop.removeCart());
    }

    @Test
    public void test_removeCart_fromTheEnd() {
        shop.removeCart();

        Cart cart = new Cart();
        shop.addCart(cart);

        assertSame(cart, shop.removeCart());
    }

    @Test
    public void test_removeCart_allCart() {
        for (int i = 0; i < MAX_CARTS; i++)
            shop.removeCart();

        assertNull(shop.removeCart());
    }

    @Test
    public void test_addGood() {
        shop.addGood(Good.MILK, 10);

        assertEquals(10, shop.countOf(Good.MILK));
        assertEquals(0, shop.countOf(Good.BREAD));
        assertEquals(0, shop.countOf(Good.MEAT));
    }

    @Test
    public void test_addGood_severalTimes() {
        shop.addGood(Good.MILK, 10);
        shop.addGood(Good.MILK, 5);
        shop.addGood(Good.MILK, 7);

        assertEquals(22, shop.countOf(Good.MILK));
        assertEquals(0, shop.countOf(Good.BREAD));
        assertEquals(0, shop.countOf(Good.MEAT));
    }

    @Test
    public void test_addSomeGood_severalTimes() {
        shop.addGood(Good.MILK, 10);
        shop.addGood(Good.BREAD, 5);
        shop.addGood(Good.MEAT, 7);

        assertEquals(10, shop.countOf(Good.MILK));
        assertEquals(5, shop.countOf(Good.BREAD));
        assertEquals(7, shop.countOf(Good.MEAT));
    }

    @Test
    public void test_removeGood() {
        shop.addGood(Good.MILK, 10);
        shop.removeGood(Good.MILK, 3);

        assertEquals(7, shop.countOf(Good.MILK));
    }

    @Test
    public void test_removeGood_severalTimes() {
        shop.addGood(Good.MILK, 10);
        shop.removeGood(Good.MILK, 3);
        shop.removeGood(Good.MILK, 2);

        assertEquals(5, shop.countOf(Good.MILK));
    }

    @Test
    public void test_removeGood_exceeded() {
        shop.addGood(Good.MILK, 10);
        int count_1 = shop.removeGood(Good.MILK, 3);
        int count_2 = shop.removeGood(Good.MILK, 2);
        int count_3 = shop.removeGood(Good.MILK, 10);

        assertEquals(count_1, 3);
        assertEquals(count_2, 2);
        assertEquals(count_3, 5);
        assertEquals(0, shop.countOf(Good.MILK));
    }

    @Test
    public void test_removeSomeGood_exceeded() {
        shop.addGood(Good.MILK, 10);
        shop.addGood(Good.BREAD, 5);
        shop.addGood(Good.MEAT, 7);

        shop.removeGood(Good.MILK,5);
        shop.removeGood(Good.BREAD,2);
        shop.removeGood(Good.MEAT,3);

        assertEquals(5, shop.countOf(Good.MILK));
        assertEquals(3, shop.countOf(Good.BREAD));
        assertEquals(4, shop.countOf(Good.MEAT));
    }
}
