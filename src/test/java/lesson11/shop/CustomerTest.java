package lesson11.shop;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-09
 */
public class CustomerTest {

    private BankCard bankCard;
    private Shop shop;
    private Cart cart = new Cart();

    @Before
    public void setUp() {
        shop = Mockito.mock(Shop.class);
        bankCard = new BankCard();
    }

    @Test
    public void test_takeCart() {
        Customer customer = new Customer(bankCard, shop);
        customer.takeCart();

        when(shop.removeCart()).thenReturn(cart);

        verify(shop, only()).removeCart();
    }

    @Test(expected = Exception.class)
    public void test_takeGood_when_cartIsNull() throws Exception {
        Customer customer = new Customer(bankCard, shop);
        customer.takeGood(Good.MILK, 1);
    }

    @Test
    public void test_takeGood_when_cartIsNotNull() throws Exception {
        Cart cart = mock(Cart.class);
        when(shop.removeCart()).thenReturn(cart);
        when(shop.removeGood(Good.MILK, 5)).thenReturn(3);

        Customer customer = new Customer(bankCard, shop);
        customer.takeCart();
        customer.takeGood(Good.MILK, 5);

        verify(shop, times(1)).removeGood(Good.MILK, 5);
        verify(cart, times(3)).addGood(Good.MILK);
    }

    @Test
    public void test_takeGood_when_cartIsNotNull_and_someGoods() throws Exception {
        Cart cart = mock(Cart.class);
        when(shop.removeCart()).thenReturn(cart);
        when(shop.removeGood(Good.BREAD, 3)).thenReturn(3);
        when(shop.removeGood(Good.MILK, 5)).thenReturn(3);
        when(shop.removeGood(Good.MEAT, 2)).thenReturn(2);

        Customer customer = new Customer(bankCard, shop);
        customer.takeCart();
        customer.takeGood(Good.BREAD, 3);
        customer.takeGood(Good.MILK, 5);
        customer.takeGood(Good.MEAT, 2);

        verify(shop, times(1)).removeGood(Good.BREAD, 3);
        verify(shop, times(1)).removeGood(Good.MILK, 5);
        verify(shop, times(1)).removeGood(Good.MEAT, 2);
        verify(cart, times(3)).addGood(Good.BREAD);
        verify(cart, times(3)).addGood(Good.MILK);
        verify(cart, times(2)).addGood(Good.MEAT);
    }

    @Test
    public void test_pay() {
        Cart cart = mock(Cart.class);
        List<Good> goods = Collections.emptyList();
        when(cart.clear()).thenReturn(goods);
        when(shop.removeCart()).thenReturn(cart);
        when(shop.calculateSum(goods)).thenReturn(1000);

        Customer customer = new Customer(bankCard, shop);
        customer.takeCart();
        customer.pay();

        ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);
        verify(shop).pay(anyObject(), captor.capture());

        verify(shop, times(1)).calculateSum(goods);
        verify(shop, times(1)).pay(anyObject(), eq(1000));
        assertEquals(1000, captor.getValue().intValue());
    }
}
