package lesson11.junit.annotation;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.TestName;
import org.junit.rules.Timeout;

import java.io.IOException;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-01
 */
public class RuleExample {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public TestName testName = new TestName();

    @Rule
    public Timeout timeout = Timeout.millis(1000);

    @Test
    public void test_writeRead() {
        try {
            FileUtil.writeAndRead(folder.newFile(), "Hello, JUnit");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_testName() {
        System.out.println(testName.getMethodName());
    }

    @Test
    public void test_timeout() {
        try {
            Thread.sleep(900);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
