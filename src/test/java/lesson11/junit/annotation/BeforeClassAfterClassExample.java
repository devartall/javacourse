package lesson11.junit.annotation;

import org.junit.*;

import java.sql.Connection;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-01
 */
public class BeforeClassAfterClassExample {

    private static Connection connection;

    @BeforeClass
    public static void setUp() {
        System.out.println("Создание соединения с БД...");
    }

    @AfterClass
    public static void reset() {
        System.out.println("Закрытие соединения с БД...");
    }

    @Test
    public void test_first() {
        System.out.println("test_first()");
    }

    @Test
    public void test_second() {
        System.out.println("test_second()");
    }
}
