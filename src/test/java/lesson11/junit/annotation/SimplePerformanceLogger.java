package lesson11.junit.annotation;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-01
 */
public class SimplePerformanceLogger implements TestRule {

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                long before = System.currentTimeMillis();
                base.evaluate();
                System.out.println("Elapsed time: " + (System.currentTimeMillis() - before));
            }
        };
    }
}
