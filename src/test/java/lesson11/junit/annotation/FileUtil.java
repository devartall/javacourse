package lesson11.junit.annotation;

import java.io.*;
import java.util.StringJoiner;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-01
 */
public class FileUtil {

    static void writeAndRead(File file, String text) {
        System.out.println("In:  " + text);
        try(FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringJoiner joiner = new StringJoiner("");
        joiner.add("Out: ");

        try(FileReader fileReader = new FileReader(file)) {
            int character = fileReader.read();
            while(character != -1) {
                joiner.add(Character.toString((char)character));
                character = fileReader.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(joiner);
    }
}
