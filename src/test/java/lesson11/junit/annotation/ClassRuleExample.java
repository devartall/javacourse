package lesson11.junit.annotation;

import org.junit.ClassRule;
import org.junit.Test;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-01
 */
public class ClassRuleExample {

    @ClassRule
    public static SimplePerformanceLogger logger = new SimplePerformanceLogger();

    @Test
    public void test_first() {
        System.out.println("test_first()");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_second() {
        System.out.println("test_second()");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
