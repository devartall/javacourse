package lesson11.junit.annotation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static junit.framework.TestCase.assertNotNull;

/**
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-01
 */
public class BeforeAfterExample {

    private static final String PATHNAME = "/Users/burkin/Documents/junit_file";

    private File file;

    @Before
    public void setUp() {
        file = new File(PATHNAME);
        if (!file.exists())
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    @After
    public void tearDown() {
        System.out.println("Удаление файла...");
        file.delete();
    }

    @Test
    public void test_writeRead_1() {
        assertNotNull(file);
        FileUtil.writeAndRead(file, "Hello, JUnit");
    }

    @Test
    public void test_writeRead_2() {
        assertNotNull(file);
        FileUtil.writeAndRead(file, "Hello, JUnit");
    }
}
