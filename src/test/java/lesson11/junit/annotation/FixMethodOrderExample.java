package lesson11.junit.annotation;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-01
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FixMethodOrderExample {

    @Test
    public void c() {
        System.out.println("3");
    }

    @Test
    public void b() {
        System.out.println("2");
    }

    @Test
    public void a() {
        System.out.println("1");
    }
}
