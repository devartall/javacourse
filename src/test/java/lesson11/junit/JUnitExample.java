package lesson11.junit;

import lesson11.junit.annotation.*;
import org.junit.internal.TextListener;
import org.junit.internal.builders.JUnit4Builder;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-28
 */
public class JUnitExample {

    static void runTest() throws InitializationError {
        JUnitCore core = new JUnitCore();
        core.addListener(new TextListener(System.out));
        Result result = core.run(new BlockJUnit4ClassRunner(CalculatorTest.class));
    }

    static void runGroupTest() throws InitializationError {
        JUnitCore core = new JUnitCore();
        core.addListener(new TextListener(System.out));

        Class[] classes = {
            BeforeAfterExample.class,
            BeforeClassAfterClassExample.class,
            RuleExample.class,
            ClassRuleExample.class,
            FixMethodOrderExample.class
        };

        Suite suite = new Suite(new JUnit4Builder(), classes);

        Result result = core.run(suite);
    }

    public static void main(String[] args) throws InitializationError {
        //runTest();

        runGroupTest();
    }
}
