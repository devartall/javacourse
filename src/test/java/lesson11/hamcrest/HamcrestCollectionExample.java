package lesson11.hamcrest;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-06
 */
public class HamcrestCollectionExample {

    @Test
    public void hasItemInArray_example() {
        assertThat(HamcrestUtils.getArray(), hasItemInArray(1));
    }

    @Test
    public void arrayContaining_example() {
        assertThat(HamcrestUtils.getArray(), arrayContaining(1, 2, 3));
    }

    @Test
    public void arrayContainingInAnyOrder_example() {
        assertThat(HamcrestUtils.getArray(), arrayContainingInAnyOrder(3, 2, 1));
    }

    @Test
    public void arrayWithSize_example() {
        assertThat(HamcrestUtils.getArray(), arrayWithSize(3));
    }

    @Test
    public void emptyArray_example() {
        assertThat(HamcrestUtils.getArray(), Matchers.not(emptyArray()));
    }

    @Test
    public void in_example() {
        assertThat(2, in(new Integer[] {1, 2, 3}));
    }

    @Test
    public void hasSize_example() {
        List<String> list = HamcrestUtils.getStrings();
        assertThat(list, hasSize(3));
    }

    @Test
    public void contains_example() {
        List<Integer> list = HamcrestUtils.getIntegers();
        assertThat(list, contains(2, 4, 6));
    }

    @Test
    public void hasItem_example() {
        List<Integer> list = HamcrestUtils.getIntegers();
        assertThat(list, Matchers.hasItem(4));
    }

    @Test
    public void hasKey_example() {
        assertThat(HamcrestUtils.getMap(), hasKey(1));
    }

    @Test
    public void hasValue_example() {
        assertThat(HamcrestUtils.getMap(), hasValue("a"));
    }

    @Test
    public void hasEntry_example() {
        assertThat(HamcrestUtils.getMap(), hasEntry(3, "c"));
    }
}
