package lesson11.hamcrest;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToCompressingWhiteSpace;
import static org.hamcrest.Matchers.equalToIgnoringCase;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-08
 */
public class HamcrestStringExample {

    @Test
    public void equalToIgnoringCase_example() {
        assertThat(HamcrestUtils.getUppercaseString(), equalToIgnoringCase("string"));
    }

    @Test
    public void equalToCompressingWhiteSpace_example() {
        assertThat(" a  b    c  ", equalToCompressingWhiteSpace("a b c"));
    }
}
