package lesson11.hamcrest;

import org.junit.Test;

import static lesson11.hamcrest.HamcrestUtils.isOne;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-04
 */
public class HamcrestCoreExample {

    @Test
    public void is_example() {
        assertThat(HamcrestUtils.getValue(), is(1));
        assertThat(HamcrestUtils.getNull(), is(nullValue()));
        assertThat(HamcrestUtils.getNull(), is(is(nullValue())));
        assertThat(HamcrestUtils.getNull(), nullValue());
    }

    @Test
    public void equalTo_example() {
        assertThat(HamcrestUtils.getValue(), equalTo(1));
    }

    @Test
    public void anyOf_example() {
        assertThat(HamcrestUtils.getValue(),
            anyOf(
                equalTo(1),
                equalTo(2),
                equalTo(3)
        ));
    }

    @Test
    public void allOf_example() {
        assertThat(HamcrestUtils.getValue(),
            allOf(
                greaterThan(-1),
                not(0),
                lessThan(2)
        ));
    }

    @Test
    public void isInstanceOf_example() {
        assertThat(HamcrestUtils.getString(), instanceOf(String.class));
    }

    @Test
    public void both_example() {
        assertThat(HamcrestUtils.getValue(), both(greaterThan(-1)).and(lessThan(2)));
    }

    @Test
    public void either_example() {
        assertThat(HamcrestUtils.getValue(), either(greaterThan(-1)).or(lessThan(1)));
    }

    @Test
    public void everyItem_example() {
        assertThat(HamcrestUtils.getStrings(), everyItem(startsWith("start_")));
    }

    @Test
    public void isAnything_example() {
        assertThat(HamcrestUtils.getValue(), anything());
    }

    @Test
    public void hasItem_example() {
        assertThat(HamcrestUtils.getStrings(), hasItems("start_A"));
    }

    @Test
    public void not_example() {
        assertThat(HamcrestUtils.getStrings(), not(hasItems("start_D")));
    }

    @Test
    public void notNullValue_example() {
        assertThat(HamcrestUtils.getString(), notNullValue());
    }

    @Test
    public void nullValue_example() {
        assertThat(HamcrestUtils.getNull(), nullValue());
    }

    @Test
    public void sameInstance_example() {
        Object obj = new Object();
        String str = HamcrestUtils.getString();

        assertThat(obj, sameInstance(obj));
        assertThat(str, sameInstance("string"));
    }

    @Test
    public void containsString_example() {
        assertThat(HamcrestUtils.getString(), containsString("in"));
    }

    @Test
    public void endsWith_example() {
        assertThat(HamcrestUtils.getString(), endsWith("ing"));
    }

    @Test
    public void startsWith_example() {
        assertThat(HamcrestUtils.getString(), startsWith("str"));
    }

    @Test
    public void customMatcher_example() {
        assertThat(HamcrestUtils.getValue(), isOne());
    }
}
