package lesson11.hamcrest;

import java.util.*;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-05
 */
public class HamcrestUtils {

    public static int getValue() {
        return 1;
    }

    public static Object getNull() {
        return null;
    }

    public static String getString() {
        return "string";
    }

    public static String getUppercaseString() {
        return "STRING";
    }

    public static List<String> getStrings() {
        List<String> list = new ArrayList<>();
        Collections.addAll(list, "start_A", "start_B", "start_C");

        return list;
    }

    public static Integer[] getArray() {
        return new Integer[] {1, 2, 3};
    }

    public static CustomMatcher isOne() {
        return new CustomMatcher();
    }

    public static List<Integer> getIntegers() {
        List<Integer> list = new ArrayList<>();
        Collections.addAll(list, 2, 4, 6);

        return list;
    }

    public static Map<Integer, String> getMap() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "a");
        map.put(2, "b");
        map.put(3, "c");

        return map;
    }
}
