package lesson11.hamcrest;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-12-05
 */
public class CustomMatcher extends BaseMatcher<Integer> {

    private static final Integer ONE = 1;

    @Override
    public boolean matches(Object o) {
        return o == ONE;
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue(ONE);
    }
}
