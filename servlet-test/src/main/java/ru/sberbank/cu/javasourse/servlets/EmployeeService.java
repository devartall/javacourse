package ru.sberbank.cu.javasourse.servlets;

import java.util.ArrayList;
import java.util.List;

/**
 * Простой сервис для арботы с сотрудниками
 */
public class EmployeeService {
    private static List<Employee> employees;

    static {
        employees = new ArrayList<>();
        employees.add(new Employee(0L, "firstName-0", "secondName-0", 25, "position-1"));
        employees.add(new Employee(1L, "firstName-1", "secondName-1", 26, "position-1"));
        employees.add(new Employee(2L, "firstName-2", "secondName-2", 27, "position-2"));
        employees.add(new Employee(3L, "firstName-3", "secondName-3", 28, "position-2"));
        employees.add(new Employee(4L, "firstName-4", "secondName-4", 29, "position-3"));
    }

    public static Employee findOne(Long id) {
        return employees.stream()
                        .filter(employee -> id.equals(employee.getId()))
                        .findAny()
                        .orElse(Employee.EMPTY_EMPLOYEE);
    }

    public static List<Employee> findAll() {
        return employees;
    }
}
