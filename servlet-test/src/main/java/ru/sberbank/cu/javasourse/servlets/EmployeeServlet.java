package ru.sberbank.cu.javasourse.servlets;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/employees")
public class EmployeeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("GET request from client");
        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        List<Employee> allEmployees;

        System.out.println("id = " + req.getParameter("id"));
        if (req.getParameter("id") == null || req.getParameter("id").isEmpty()) {
            allEmployees = EmployeeService.findAll();
        } else {
            String id = req.getParameter("id");
            Long value = Long.parseLong(id);
            allEmployees = Collections.singletonList(EmployeeService.findOne(value));
        }

        String stringifiedJsonArray = "";

        try {
            JSONArray jsonEmployeesList = new JSONArray();
            for (Employee employee : allEmployees) {
                JSONObject jsonEmployee = new JSONObject();
                jsonEmployee.put("id", employee.getId());
                jsonEmployee.put("firstName", employee.getFirstName());
                jsonEmployee.put("secondName", employee.getSecondName());
                jsonEmployee.put("age", employee.getAge());
                jsonEmployee.put("position", employee.getPosition());
                jsonEmployeesList.put(jsonEmployee);
            }
            stringifiedJsonArray = jsonEmployeesList.toString();
        } catch (JSONException jsonEx) {
            System.err.println("Error during json serialization: " + jsonEx.toString());
        }

        out.print(stringifiedJsonArray);
        //out.print(createHtmlResponse());
        out.flush();
    }
}
