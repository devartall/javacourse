package ru.sberbank.cu.javasourse.servlets;

public class Employee {
    private Long id;
    private String firstName;
    private String secondName;
    private int age;
    private String position;

    public Employee(Long id, String firstName, String secondName, int age, String position) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
        this.position = position;
    }

    public static final Employee EMPTY_EMPLOYEE = new Employee(-1L, "", "", 0, "");

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public boolean isEmpty() {
        return EMPTY_EMPLOYEE.equals(this);
    }
}
