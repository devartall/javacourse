package ru.sberbank.cu.javasourse.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/employees-html")
public class TestServlet extends HttpServlet {

    /**
     * Переопределение метода обработки GET запросов
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("GET request from client");
        PrintWriter out = resp.getWriter();
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");

        String employeesList = "<html><HEAD>\n" +
                "<TITLE>Your Title Here</TITLE>\n" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"FFFFFF\">";

        List <String> employeesAsHtml;
        List<Employee> allEmployees;

        if (req != null && req.getParameter("id").isEmpty()) {
            allEmployees = EmployeeService.findAll();
        } else {
            String id = req.getParameter("id");
            Long value = Long.parseLong(id);
            allEmployees = Collections.singletonList(EmployeeService.findOne(value));
        }

        employeesAsHtml = allEmployees.stream()
                                      .map(this::wrapAsHtml)
                                      .collect(Collectors.toList());

        for (String record : employeesAsHtml) {
            employeesList = employeesList.concat(record);
        }

        employeesList = employeesList.concat("</BODY></HTML>");
        out.print(employeesList);
        //out.print(createHtmlResponse());
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    private String createHtmlResponse() {
        return "<HTML>\n" +
                "\n" +
                "<HEAD>\n" +
                "\n" +
                "<TITLE>" + new Date().toString() + "</TITLE>\n" +
                "\n" +
                "</HEAD>\n" +
                "\n" +
                "<BODY BGCOLOR=\"FFFFFF\">\n" +
                "\n" +
                "<CENTER><IMG SRC=\"clouds.jpg\" ALIGN=\"BOTTOM\"> </CENTER>\n" +
                "\n" +
                "<HR>\n" +
                "\n" +
                "<a href=\"http://somegreatsite.com\">Link Name</a>\n" +
                "\n" +
                "is a link to another nifty site\n" +
                "\n" +
                "<H1>" + new Date().toString() +  "</H1>\n" +
                "\n" +
                "<H2>This is a Medium Header</H2>\n" +
                "\n" +
                "Send me mail at <a href=\"mailto:support@yourcompany.com\">\n" +
                "\n" +
                "support@yourcompany.com</a>.\n" +
                "\n" +
                "<P> This is a new paragraph!\n" +
                "\n" +
                "<P> <B>This is a new paragraph!</B>\n" +
                "\n" +
                "<BR> <B><I>This is a new sentence without a paragraph break, in bold italics.</I></B>\n" +
                "\n" +
                "<HR>\n" +
                "\n" +
                "</BODY>\n" +
                "\n" +
                "</HTML>";
    }

    private String wrapAsHtml(Employee employee) {
        if (employee.isEmpty()) {
            return "<p>No employee found</p>";
        }
        return "<p> <b>id </b> " + employee.getId()
                + " | <b>first name </b> " + employee.getFirstName()
                + " | <b>second name </b> " + employee.getSecondName()
                + " | <b>age </b> " + employee.getAge()
                + " | <b>position </b> " + employee.getPosition()
                + "</p>";
    }
}
