package javacourse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("rest")
public class TestController {

    @GetMapping
    public ResponseEntity getRequest() {
        return new ResponseEntity("some text", HttpStatus.OK);
    }
}
